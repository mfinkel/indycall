create table adv_ayetstudios
(
    aye_id           int auto_increment
        primary key,
    aye_transaction  varchar(45)      null,
    aye_amount       float(10, 4)     null,
    aye_device_id    varchar(45)      null,
    aye_date         datetime         null,
    aye_publisher_id int              null,
    aye_status       char default 'N' not null
)
    charset = utf8;

create table blocked_numbers
(
    bn_id       int auto_increment
        primary key,
    bn_pub_id   int                                  not null,
    bn_phone    varchar(20)                          null,
    bn_date_bgn datetime default current_timestamp() null,
    bn_date_exp datetime                             not null
)
    collate = utf8_unicode_ci;

create index date_exp_idx
    on blocked_numbers (bn_date_exp);

create index pub_id_idx
    on blocked_numbers (bn_pub_id);

create table doctrine_migration_versions
(
    version        varchar(191) not null
        primary key,
    executed_at    datetime(6)  null,
    execution_time int          null
)
    collate = utf8_unicode_ci;

create table mask_numbers
(
    number varchar(30) not null
)
    collate = utf8_unicode_ci;

create table mask_numbers_tmp
(
    number varchar(30) not null
)
    collate = utf8_unicode_ci;

create table payments_android
(
    poa_id           int auto_increment
        primary key,
    poa_amount       decimal(10, 2) not null,
    poa_currency     varchar(3)     not null,
    poa_publisher_id int            not null,
    poa_state        char           not null comment 'N- new
D- done
E- error
U- unknown
S- skipped
R-refund',
    poa_date_create  datetime       not null,
    poa_date_update  datetime       null,
    poa_product_id   varchar(45)    not null comment 'sku',
    poa_receipt      blob           not null,
    poa_signature    blob           not null,
    poa_order_id     varchar(45)    not null,
    poa_order_desc   varchar(45)    null
)
    charset = utf8;

create index porder_id
    on payments_android (poa_order_id);

create index product_idx
    on payments_android (poa_product_id, poa_state);

create table payments_apple
(
    poa_id           int auto_increment
        primary key,
    poa_amount       decimal(10, 2) not null,
    poa_currency     varchar(3)     not null,
    poa_publisher_id int            not null,
    poa_state        char           not null comment 'N- new
D- done
E- error
U- unknown
S- skipped
R-refund',
    poa_date_create  datetime       not null,
    poa_date_update  datetime       null,
    poa_product_id   varchar(45)    not null comment 'sku',
    poa_receipt      blob           not null,
    poa_order_id     varchar(45)    not null,
    poa_order_desc   varchar(45)    null
)
    charset = utf8;

create index porder_id
    on payments_apple (poa_order_id);

create index product_idx
    on payments_apple (poa_product_id, poa_state);

create table products
(
    prod_id               varchar(45)          not null
        primary key,
    prod_value            varchar(45)          null,
    prod_name             varchar(45)          null,
    prod_amount           decimal(10, 2)       not null,
    prod_amount_to_enroll decimal(10, 2)       null,
    prod_currency         varchar(15)          null,
    prod_enabled          enum ('Y', 'N')      not null,
    prod_order            tinyint              null,
    prod_type             enum ('1', '2', '3') null,
    prod_comments         varchar(145)         null
)
    collate = utf8_unicode_ci;

create table publisher_charges
(
    puc_id             int auto_increment
        primary key,
    puc_publisher      varchar(45)    not null,
    puc_pay_date       varchar(45)    null,
    puc_pay_type       set ('I', 'O') not null comment 'i - income
o - charge',
    puc_pay_source     varchar(3)     not null comment 'CAL - user Calls
INT - internal
PP - PayPal
GG - Google
TRA - Minute transfer
POL - Polfish
AYE -Ayetstudios
ANN - Annecy
',
    puc_balance_before decimal(12, 4) not null,
    puc_pay_amount     decimal(12, 4) not null,
    puc_balance_after  decimal(12, 4) not null,
    puc_descr          varchar(150)   null
)
    charset = utf8;

create index date_type_idx
    on publisher_charges (puc_pay_date, puc_pay_type, puc_pay_source);

create index publisher_idx
    on publisher_charges (puc_publisher);

create table publishers
(
    pub_id               int unsigned auto_increment
        primary key,
    pub_sn_id            varchar(60)                          not null,
    pub_gn_id            varchar(60)                          null comment 'Google ID',
    pub_gn_token         longtext collate utf8mb4_bin         null,
    pub_gn_refresh_token varchar(512)                         null,
    pub_data             longtext collate utf8mb4_bin         null,
    pub_date             datetime default current_timestamp() not null,
    pub_date_last        datetime default current_timestamp() null,
    pub_uniq_code        varchar(9)                           null,
    pub_tmp              varchar(40)                          null,
    constraint pub_uniq_code_UNIQUE
        unique (pub_uniq_code)
)
    collate = utf8_unicode_ci;

create index pub_gn_id_idx
    on publishers (pub_gn_id);

create index pub_gn_refresh_token
    on publishers (pub_gn_refresh_token);

create index pub_sn_id_ixd
    on publishers (pub_sn_id);

create index pub_uniq_code_idx
    on publishers (pub_uniq_code);

create table purchased_phones_history
(
    pp_id         int auto_increment
        primary key,
    pp_pub_id     int                                     not null,
    pp_date       datetime    default current_timestamp() not null,
    pp_num_before varchar(25) default ''                  null,
    pp_num_after  varchar(25)                             not null,
    pp_att_left   tinyint                                 not null
)
    collate = utf8_unicode_ci;

create table rates
(
    code_id     int auto_increment
        primary key,
    code        varchar(16)                           not null,
    codename    varchar(128)                          not null,
    rate        float(6, 4)                           not null,
    rate_date   timestamp default current_timestamp() not null,
    rate_prefix varchar(6)                            null
)
    collate = utf8_unicode_ci;

create table requests
(
    req_id         int auto_increment
        primary key,
    req_pub        int                                                    not null,
    req_date       datetime                                               not null,
    req_voip_login varchar(30)                                            not null,
    req_voip_pass  varchar(10)                                            not null,
    req_voip_ip    varchar(20)                                            not null,
    req_pub_ip     varchar(50)                                            not null,
    req_dst_number varchar(20)                                            null,
    req_src_number varchar(20)                                            null,
    req_state      enum ('NEW', 'CDR', 'UPD', 'DON', 'PRO') default 'NEW' not null
)
    collate = utf8_unicode_ci;

create index login_state_idx
    on requests (req_voip_login, req_state);

INSERT INTO indycall.publishers (pub_id, pub_sn_id, pub_gn_id, pub_gn_token, pub_gn_refresh_token, pub_data, pub_date, pub_date_last, pub_uniq_code, pub_tmp) VALUES (23369156, '1', null, null, null, '{"lastIp":"127.0.0.1","geo":{"lat":0,"lon":0},"amount":{"balance":0,"purchase_enabled":1,"called_amount":0},"prem_phone":{"number":"","date_expire":null,"enabled":1,"tryis":null},"user_area":{"enabled":1,"uniq_code_enabled":1},"gdpr":{"date":"2021-03-20","status":1}}', '2021-03-14 21:36:11', '2021-03-19 00:53:16', null, null);
