<?php

namespace App\Subscribers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class ExceptionSubscriber
{
    public function onKernelResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();
        Response::HTTP_OK === $response->getStatusCode()
            ?: $response->setStatusCode(Response::HTTP_OK);
        $event->setResponse($response);
    }
}
