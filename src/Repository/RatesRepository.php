<?php

namespace App\Repository;

use App\Services\RedisHelper;
use Doctrine\ORM\EntityRepository;

class RatesRepository extends EntityRepository
{
    public function getAllRates(): array
    {
        $this->getEntityManager()->getConnection('read');
        $query = $this->createQueryBuilder('r')
            ->select('r')
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(RedisHelper::MAX_TTL, 'rates:list');

        return $query->getArrayResult();
    }
}
