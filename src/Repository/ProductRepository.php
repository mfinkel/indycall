<?php

namespace App\Repository;

use App\Entity\Product;
use App\Services\RedisHelper;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class ProductRepository extends EntityRepository
{
    /**
     * @return Product[]
     */
    public function getAllEnabledProducts(): array
    {
        $query = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.isEnabled = :isEnabled')
            ->andWhere('p.productType = :productType')
            ->setParameter('isEnabled', Product::PRODUCT_TYPE_ENABLED)
            ->setParameter('productType', 1)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(RedisHelper::MAX_TTL, 'products:list');

        return $query->getArrayResult();
    }

    public function getRenewNumber(string $productId): array
    {
        $query = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.id = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(RedisHelper::MAX_TTL, "products:{$productId}");

        return $query->getArrayResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getProductById(string $productId)
    {
        $query = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.id = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(RedisHelper::MAX_TTL, 'products:list');

        return $query->getOneOrNullResult();
    }
}
