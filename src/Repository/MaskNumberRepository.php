<?php

namespace App\Repository;

use App\Services\RedisHelper;
use Doctrine\ORM\EntityRepository;

class MaskNumberRepository extends EntityRepository
{
    public function getMaskNumbers(): array
    {
        $this->getEntityManager()->getConnection('read');
        $query = $this->createQueryBuilder('mn')
            ->select('mn.number')
            ->getQuery();
//            ->useQueryCache(true)
//            ->enableResultCache(RedisHelper::MAX_TTL, 'mask:list');

        return $query->getArrayResult();
    }
}
