<?php

namespace App\Repository;

use App\Entity\BlockedNumber;
use App\Security\IndyCallUserInterface;
use Doctrine\ORM\EntityRepository;

class BlockedNumberRepository extends EntityRepository
{
    /**
     * @param IndyCallUserInterface $publisher
     */
    public function getBlockedNumberList(IndyCallUserInterface $publisher): array
    {
        $query = $this->createQueryBuilder('bn')
            ->select('bn.blockedNumber')
            ->where('bn.publisherId = :publisher')
            ->setParameter('publisher', $publisher->getId())
            ->getQuery();
//            ->useQueryCache(true)
//            ->enableResultCache(3600, BlockedNumber::BLOCKED_NUMBER_LIST.$publisher->getId());

        return $query->getArrayResult();
    }

    /**
     * @param IndyCallUserInterface $publisher
     */
    public function countAvailableSlots(IndyCallUserInterface $publisher): array
    {
        $query = $this->createQueryBuilder('bn')
            ->where('bn.publisherId = :publisher')
            ->setParameter('publisher', $publisher->getId())
            ->getQuery();
//            ->useQueryCache(true)
//            ->enableResultCache(3600, BlockedNumber::BLOCKED_NUMBER_AVAILABLE_COUNT.$publisher->getId());

        return $query->getResult();
    }

    /**
     * @param IndyCallUserInterface $publisher
     */
    public function countPurchasedSlots(IndyCallUserInterface $publisher): array
    {
        $query = $this->createQueryBuilder('bn')
            ->where('bn.publisherId = :publisher')
            ->andWhere('bn.blockedNumber is NULL OR bn.blockedNumber = :empty')
            ->setParameter('publisher', $publisher->getId())
            ->setParameter('empty', '')
            ->getQuery();
//            ->useQueryCache(true)
//            ->enableResultCache(3600, BlockedNumber::BLOCKED_NUMBER_PURCHASE_COUNT.$publisher->getId());

        return $query->getArrayResult();
    }
}
