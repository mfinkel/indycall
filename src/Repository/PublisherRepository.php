<?php

namespace App\Repository;

use App\Entity\Publisher;
use Doctrine\ORM\EntityRepository;

class PublisherRepository extends EntityRepository
{
    /**
     * @return Publisher[]
     */
    public function getPublisherUniqueCodes(): array
    {
        $query = $this->createQueryBuilder('p')->select('p.uniqueCode')->getQuery();

        return $query->getResult();
    }

    public function findPublisherByToken(string $shouldBeAToken)
    {
        $query = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.googleId = :token')
            ->orWhere('p.googleRefreshToken = :token')
            ->setParameter('token', $shouldBeAToken)
            ->getQuery();

        return $query->getSingleResult();
    }
}
