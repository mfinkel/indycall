<?php

namespace App\Repository;

use App\Entity\CallDetailRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CallDetailRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method CallDetailRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method CallDetailRecord[]    findAll()
 * @method CallDetailRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CallDetailRecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CallDetailRecord::class);
    }

     /**
      * @return CallDetailRecord[] Returns an array of CallDetailRecord objects
      */
    public function findByExampleField($value): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneById($id): ?CallDetailRecord
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getLastId()
    {
        return $this->createQueryBuilder('c')
            ->select('MAX(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
