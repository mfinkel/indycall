<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class PaymentsAndroidRepository extends EntityRepository
{
    public function removeTestPurchase(string $poaOrderId): array
    {
        $this->getEntityManager()->getConnection('write');
        $query = $this->createQueryBuilder('pa')
            ->delete()
            ->where('pa.poaOrderId = :poaOrderId')
            ->setParameter('poaOrderId', $poaOrderId)
            ->getQuery();

        return $query->getResult();
    }

    public function isOrderExist(string $poaOrderId): int
    {
        $this->getEntityManager()->getConnection('write');
        $query = $this->createQueryBuilder('pa')
            ->select('COUNT(pa.poaId)')
            ->where('pa.poaOrderId = :poaOrderId')
            ->setParameter('poaOrderId', $poaOrderId)
            ->getQuery();

        return $query->getSingleScalarResult();
    }
}
