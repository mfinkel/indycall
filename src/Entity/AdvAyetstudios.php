<?php

namespace App\Entity;

use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdvAyetstudios.
 *
 * @ORM\Table(name="adv_ayetstudios")
 * @ORM\Entity
 */
class AdvAyetstudios
{
    /**
     * @ORM\Column(name="aye_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $ayeId;

    /**
     * @ORM\Column(name="aye_transaction", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private ?string $ayeTransaction = null;

    /**
     * @ORM\Column(name="aye_amount", type="float", precision=10, scale=4, nullable=true, options={"default"="0"})
     */
    private ?float $ayeAmount = null;

    /**
     * @ORM\Column(name="aye_device_id", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private ?string $ayeDeviceId = null;

    /**
     * @var DateTimeInterface|Carbon|null
     *
     * @ORM\Column(name="aye_date", type="datetime", nullable=true)
     */
    private DateTimeInterface | Carbon | null $ayeDate;

    /**
     * @ORM\Column(name="aye_publisher_id", type="integer")
     */
    private ?int $ayePublisherId = null;

    /**
     * @ORM\Column(name="aye_status", type="string", length=1, nullable=false, options={"default"="N","fixed"=true})
     */
    private string $ayeStatus = 'N';

    public function getAyeId(): ?int
    {
        return $this->ayeId;
    }

    public function getAyeTransaction(): ?string
    {
        return $this->ayeTransaction;
    }

    /**
     * @return $this
     */
    public function setAyeTransaction(?string $ayeTransaction): self
    {
        $this->ayeTransaction = $ayeTransaction;

        return $this;
    }

    public function getAyeAmount(): ?float
    {
        return $this->ayeAmount;
    }

    /**
     * @return $this
     */
    public function setAyeAmount(?float $ayeAmount): self
    {
        $this->ayeAmount = $ayeAmount;

        return $this;
    }

    public function getAyeDeviceId(): ?string
    {
        return $this->ayeDeviceId;
    }

    /**
     * @return $this
     */
    public function setAyeDeviceId(?string $ayeDeviceId): self
    {
        $this->ayeDeviceId = $ayeDeviceId;

        return $this;
    }

    /**
     * @return DateTimeInterface|Carbon|null
     */
    public function getAyeDate(): DateTimeInterface | Carbon | null
    {
        return $this->ayeDate;
    }

    /**
     * @param DateTimeInterface|Carbon|null $ayeDate
     *
     * @return $this
     */
    public function setAyeDate(DateTimeInterface | Carbon | null $ayeDate): AdvAyetstudios
    {
        $this->ayeDate = $ayeDate;

        return $this;
    }

    public function getAyePublisherId(): ?int
    {
        return $this->ayePublisherId;
    }

    /**
     * @return $this
     */
    public function setAyePublisherId(?int $ayePublisherId): self
    {
        $this->ayePublisherId = $ayePublisherId;

        return $this;
    }

    public function getAyeStatus(): ?string
    {
        return $this->ayeStatus;
    }

    /**
     * @return $this
     */
    public function setAyeStatus(string $ayeStatus): self
    {
        $this->ayeStatus = $ayeStatus;

        return $this;
    }
}
