<?php

namespace App\Entity;

use App\Repository\CallDetailRecordRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="cdr")
 * @ORM\Entity(repositoryClass=CallDetailRecordRepository::class)
 */
class CallDetailRecord
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @@ORM\Column(name="date", type="datetime")
     */
    private DateTime $date;

    /**
     * @@ORM\Column(name="pub_id", type="integer")
     */
    private int $pubId;

    /**
     * @@ORM\Column(name="pub_ip", type="string", length="15")
     */
    private string $pubIp;

    /**
     * @@ORM\Column(name="call_from", type="string", length="25")
     */
    private string $callFrom;

    /**
     * @@ORM\Column(name="call_to", type="string", length="25")
     */
    private string $callTo;

    /**
     * @@ORM\Column(name="call_duration", type="integer")
     */
    private int $callDuration;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return CallDetailRecord
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     *
     * @return CallDetailRecord
     */
    public function setDate(DateTime $date): CallDetailRecord
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getPubId(): int
    {
        return $this->pubId;
    }

    /**
     * @param int $pubId
     *
     * @return CallDetailRecord
     */
    public function setPubId(int $pubId): CallDetailRecord
    {
        $this->pubId = $pubId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPubIp(): string
    {
        return $this->pubIp;
    }

    /**
     * @param string $pubIp
     *
     * @return CallDetailRecord
     */
    public function setPubIp(string $pubIp): CallDetailRecord
    {
        $this->pubIp = $pubIp;

        return $this;
    }

    /**
     * @return string
     */
    public function getCallFrom(): string
    {
        return $this->callFrom;
    }

    /**
     * @param string $callFrom
     *
     * @return CallDetailRecord
     */
    public function setCallFrom(string $callFrom): CallDetailRecord
    {
        $this->callFrom = $callFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getCallTo(): string
    {
        return $this->callTo;
    }

    /**
     * @param string $callTo
     *
     * @return CallDetailRecord
     */
    public function setCallTo(string $callTo): CallDetailRecord
    {
        $this->callTo = $callTo;

        return $this;
    }

    /**
     * @return int
     */
    public function getCallDuration(): int
    {
        return $this->callDuration;
    }

    /**
     * @param int $callDuration
     *
     * @return CallDetailRecord
     */
    public function setCallDuration(int $callDuration): CallDetailRecord
    {
        $this->callDuration = $callDuration;

        return $this;
    }
}
