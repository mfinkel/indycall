<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublisherCharges.
 *
 * @ORM\Table(name="publisher_charges",
 *     indexes={
 *      @ORM\Index(name="publisher_idx", columns={"puc_publisher"}),
 *      @ORM\Index(name="date_type_idx", columns={"puc_pay_date", "puc_pay_type", "puc_pay_source"} )
 *     }
 * )
 * @ORM\Entity
 */
class PublisherCharge
{
    /**
     * @ORM\Column(name="puc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="puc_publisher", type="string", length=45, nullable=false)
     */
    private string $publisherId;

    /**
     * @ORM\Column(name="puc_pay_date", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private ?string $payDate = null;

    /**
     * @ORM\Column(
     *     name="puc_pay_type",
     *     type="string",
     *     length=1,
     *     nullable=false,
     *     options={"comment"="i-income, o-charge"}
     * )
     */
    private string $paymentType;

    /**
     * @ORM\Column(
     *     name="puc_pay_source",
     *     type="string",
     *     length=3,
     *     nullable=false,
     *     options={
     *          "comment"="
     *              CAL-user Calls,
     *              INT-internal,
     *              PP-PayPal,
     *              GG-Google,
     *              TRA-Minute transfer,
     *              POL-Polfish,
     *              AYE -Ayetstudios,
     *              ANN-Annecy
     *          "
     *      }
     * )
     */
    private string $paymentSource;

    /**
     * @ORM\Column(name="puc_balance_before", type="decimal", precision=12, scale=4, nullable=false)
     */
    private string $balanceBefore;

    /**
     * @ORM\Column(name="puc_pay_amount", type="decimal", precision=12, scale=4, nullable=false)
     */
    private string $amount;

    /**
     * @ORM\Column(name="puc_balance_after", type="decimal", precision=12, scale=4, nullable=false)
     */
    private string $balanceAfter;

    /**
     * @ORM\Column(name="puc_descr", type="string", length=150, nullable=true, options={"default"="NULL"})
     */
    private ?string $description = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublisherId(): ?string
    {
        return $this->publisherId;
    }

    public function setPublisherId(string $publisherId): self
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    public function getPayDate(): ?string
    {
        return $this->payDate;
    }

    public function setPayDate(?string $payDate): self
    {
        $this->payDate = $payDate;

        return $this;
    }

    public function getPaymentType(): string
    {
        return $this->paymentType;
    }

    public function setPaymentType(string $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getPaymentSource(): ?string
    {
        return $this->paymentSource;
    }

    public function setPaymentSource(string $paymentSource): self
    {
        $this->paymentSource = $paymentSource;

        return $this;
    }

    public function getBalanceBefore(): ?string
    {
        return $this->balanceBefore;
    }

    public function setBalanceBefore(string $balanceBefore): self
    {
        $this->balanceBefore = $balanceBefore;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getBalanceAfter(): ?string
    {
        return $this->balanceAfter;
    }

    public function setBalanceAfter(string $balanceAfter): self
    {
        $this->balanceAfter = $balanceAfter;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
