<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rates.
 *
 * @ORM\Table(name="sip_login_data")
 * @ORM\Entity
 */
class SipLoginData
{
    public const CACHE_ID = 'SIP_LOGIN_CACHE:';

    /**
     * @ORM\Column(name="code_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="publisher_id", type="integer", nullable=false)
     */
    private int $publisherId;

    /**
     * @ORM\Column(name="request_sip_login", type="string", nullable=false)
     */
    private string $requestSipLogin;

    /**
     * @ORM\Column(name="request_sip_pass", type="string", nullable=false)
     */
    private string $requestSipPass;

    /**
     * @ORM\Column(name="request_phone_src", type="string", nullable=false)
     */
    private string $requestPhoneSrc;

    /**
     * @ORM\Column(name="request_phone_dst", type="string", nullable=true)
     */
    private ?string $requestPhoneDst = null;

    /**
     * @ORM\Column(name="request_call_seconds", type="string", nullable=false)
     */
    private string $requestCallSeconds;

    /**
     * @ORM\Column(name="request_route", type="string", nullable=false)
     */
    private string $requestRoute;

    /**
     * @ORM\Column(name="publisher_purchased_amount", type="string", nullable=false)
     */
    private string $publisherPurchasedAmount;

    /**
     * @ORM\Column(name="publisher_total_loss", type="string", nullable=true)
     */
    private ?string $publisherTotalLoss = null;

    /**
     * @ORM\Column(name="call_prefix", type="string", nullable=false)
     */
    private string $callPrefix;

    /**
     * @ORM\Column(name="dead_man", type="string", nullable=false)
     */
    private string $deadMan;

    /**
     * @ORM\Column(name="block_id", type="string", nullable=true)
     */
    private ?string $blockId = null;

    public function __toString(): string
    {
        return json_encode(
            [
                'publisherId' => $this->publisherId,
                'request_sip_login' => $this->requestSipLogin,
                'request_sip_pass' => $this->requestSipPass,
                'request_phone_src' => $this->requestPhoneSrc,
                'request_phone_dst' => $this->requestPhoneDst,
                'request_call_seconds' => $this->requestCallSeconds,
                'request_route' => $this->requestRoute,
                'publisher_purchased_amount' => $this->publisherPurchasedAmount,
                'publisher_total_loss' => $this->publisherTotalLoss,
                'call_prefix' => $this->callPrefix,
                'deadman' => $this->deadMan,
                'block_id' => $this->blockId,
            ],
            JSON_THROW_ON_ERROR
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): SipLoginData
    {
        $this->id = $id;

        return $this;
    }

    public function getPublisherId(): int
    {
        return $this->publisherId;
    }

    public function setPublisherId(int $publisherId): SipLoginData
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    public function getRequestSipLogin(): string
    {
        return $this->requestSipLogin;
    }

    public function setRequestSipLogin(string $requestSipLogin): SipLoginData
    {
        $this->requestSipLogin = $requestSipLogin;

        return $this;
    }

    public function getRequestSipPass(): string
    {
        return $this->requestSipPass;
    }

    public function setRequestSipPass(string $requestSipPass): SipLoginData
    {
        $this->requestSipPass = $requestSipPass;

        return $this;
    }

    public function getRequestPhoneSrc(): string
    {
        return $this->requestPhoneSrc;
    }

    public function setRequestPhoneSrc(string $requestPhoneSrc): SipLoginData
    {
        $this->requestPhoneSrc = $requestPhoneSrc;

        return $this;
    }

    public function getRequestPhoneDst(): ?string
    {
        return $this->requestPhoneDst;
    }

    public function setRequestPhoneDst(?string $requestPhoneDst): SipLoginData
    {
        $this->requestPhoneDst = $requestPhoneDst;

        return $this;
    }

    public function getRequestCallSeconds(): string
    {
        return $this->requestCallSeconds;
    }

    public function setRequestCallSeconds(string $requestCallSeconds): SipLoginData
    {
        $this->requestCallSeconds = $requestCallSeconds;

        return $this;
    }

    public function getRequestRoute(): string
    {
        return $this->requestRoute;
    }

    public function setRequestRoute(string $requestRoute): SipLoginData
    {
        $this->requestRoute = $requestRoute;

        return $this;
    }

    public function getPublisherPurchasedAmount(): string
    {
        return $this->publisherPurchasedAmount;
    }

    public function setPublisherPurchasedAmount(string $publisherPurchasedAmount): SipLoginData
    {
        $this->publisherPurchasedAmount = $publisherPurchasedAmount;

        return $this;
    }

    public function getPublisherTotalLoss(): ?string
    {
        return $this->publisherTotalLoss;
    }

    public function setPublisherTotalLoss(?string $publisherTotalLoss): SipLoginData
    {
        $this->publisherTotalLoss = $publisherTotalLoss;

        return $this;
    }

    public function getCallPrefix(): string
    {
        return $this->callPrefix;
    }

    public function setCallPrefix(string $callPrefix): SipLoginData
    {
        $this->callPrefix = $callPrefix;

        return $this;
    }

    public function getDeadMan(): string
    {
        return $this->deadMan;
    }

    public function setDeadMan(string $deadMan): SipLoginData
    {
        $this->deadMan = $deadMan;

        return $this;
    }

    public function getBlockId(): ?string
    {
        return $this->blockId;
    }

    public function setBlockId(?string $blockId): SipLoginData
    {
        $this->blockId = $blockId;

        return $this;
    }
}
