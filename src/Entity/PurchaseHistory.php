<?php

namespace App\Entity;

use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * PurchasedPhonesHistory.
 *
 * @ORM\Table(name="purchased_phones_history")
 * @ORM\Entity
 */
class PurchaseHistory
{
    /**
     * @ORM\Column(name="pp_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $ppId;

    /**
     * @ORM\Column(name="pp_pub_id", type="integer", nullable=false)
     */
    private int $publisherId;

    /**
     * @var DateTimeInterface|Carbon
     * @ORM\Column(name="pp_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private DateTimeInterface | Carbon $createdAt;

    /**
     * @ORM\Column(name="pp_num_before", type="string", length=25, nullable=true, options={"default"="''"})
     */
    private ?string $numberBefore = '';

    /**
     * @ORM\Column(name="pp_num_after", type="string", length=25, nullable=false)
     */
    private string $newNumber;

    /**
     * @ORM\Column(name="pp_att_left", type="boolean", nullable=false)
     */
    private bool $attemptsLeft;

    /**
     * PurchasedHistory constructor.
     */
    public function __construct()
    {
        $this->createdAt = Carbon::now();
    }

    public function getPpId(): ?int
    {
        return $this->ppId;
    }

    public function getPublisherId(): ?int
    {
        return $this->publisherId;
    }

    /**
     * @return $this
     */
    public function setPublisherId(int $publisherId): self
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    /**
     * @return DateTimeInterface|Carbon|null
     */
    public function getCreatedAt(): DateTimeInterface | Carbon | null
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface|Carbon $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTimeInterface | Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getNumberBefore(): ?string
    {
        return $this->numberBefore;
    }

    /**
     * @return $this
     */
    public function setNumberBefore(?string $numberBefore): self
    {
        $this->numberBefore = $numberBefore;

        return $this;
    }

    public function getNewNumber(): ?string
    {
        return $this->newNumber;
    }

    /**
     * @return $this
     */
    public function setNewNumber(string $newNumber): self
    {
        $this->newNumber = $newNumber;

        return $this;
    }

    public function getAttemptsLeft(): ?bool
    {
        return $this->attemptsLeft;
    }

    /**
     * @return $this
     */
    public function setAttemptsLeft(bool $attemptsLeft): self
    {
        $this->attemptsLeft = $attemptsLeft;

        return $this;
    }
}
