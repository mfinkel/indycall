<?php

namespace App\Entity;

use App\Security\IndyCallUserInterface;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Publishers.
 *
 * @ORM\Table(
 *     name="publishers",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="pub_uniq_code_UNIQUE", columns={"pub_uniq_code"})
 *     },
 *     indexes={
 *          @ORM\Index(name="pub_gn_id_idx", columns={"pub_gn_id"}),
 *          @ORM\Index(name="pub_gn_refresh_token", columns={"pub_gn_refresh_token"}),
 *          @ORM\Index(name="pub_sn_id_ixd", columns={"pub_sn_id"}),
 *          @ORM\Index(name="pub_uniq_code_idx", columns={"pub_uniq_code"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PublisherRepository")
 */
class Publisher implements IndyCallUserInterface
{
    /**
     * @ORM\Column(name="pub_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="pub_sn_id", type="string", length=60, nullable=false)
     */
    private string $serialNumber;

    /**
     * @ORM\Column(
     *     name="pub_gn_id",
     *     type="string",
     *     length=60,
     *     nullable=true,
     *     options={"default"="NULL","comment"="Google ID"})
     */
    private ?string $googleId = null;

    /**
     * @ORM\Column(name="pub_gn_token", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private ?string $googleToken = null;

    /**
     * @ORM\Column(name="pub_gn_refresh_token", type="string", length=512, nullable=true, options={"default"="NULL"})
     */
    private ?string $googleRefreshToken = null;

    /**
     * @ORM\Column(name="pub_data", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private ?string $jsonData = null;

    /**
     * @var DateTimeInterface|Carbon
     *
     * @ORM\Column(name="pub_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private DateTimeInterface|Carbon $createdAt;

    /**
     * @var DateTimeInterface|Carbon|null
     *
     * @ORM\Column(name="pub_date_last", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private DateTimeInterface|null|Carbon $updatedAt;

    /**
     * @ORM\Column(name="pub_uniq_code", type="string", length=9, nullable=true, options={"default"="NULL"})
     */
    private ?string $uniqueCode = null;

    /**
     * @ORM\Column(name="pub_tmp", type="string", length=40, nullable=true, options={"default"="NULL"})
     */
    private ?string $temporary = null;

    /**
     * Publisher constructor.
     */
    public function __construct()
    {
        $this->createdAt = Carbon::now();
        $this->updatedAt = Carbon::now();
    }

    private array $roles = [];

    public function getRoles(): array
    {
        $userRole = $this->roles;
        $userRole[] = 'ROLE_USER';

        return array_unique($userRole);
    }

    public function setRoles(array $roles): self
    {
        if (!$roles) {
            $roles[] = 'ROLE_USER';
        }
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): void
    {

    }

    public function getSalt(): void
    {

    }

    public function getUsername(): string
    {
        return (string)$this->id;
    }

    public function eraseCredentials(): void
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getGoogleToken(): ?string
    {
        return $this->googleToken;
    }

    public function setGoogleToken(?string $googleToken): self
    {
        $this->googleToken = $googleToken;

        return $this;
    }

    public function getGoogleRefreshToken(): ?string
    {
        return $this->googleRefreshToken;
    }

    public function setGoogleRefreshToken(?string $googleRefreshToken): self
    {
        $this->googleRefreshToken = $googleRefreshToken;

        return $this;
    }

    public function getJsonData(): array
    {
        return json_decode($this->jsonData, true, 512, JSON_THROW_ON_ERROR);
    }

    public function setJsonData(array $jsonData): self
    {
        $this->jsonData = json_encode($jsonData, JSON_THROW_ON_ERROR);

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface|Carbon|null
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): DateTimeInterface|Carbon|null
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface|null $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUniqueCode(): ?string
    {
        return $this->uniqueCode;
    }

    public function setUniqueCode(?string $uniqueCode): self
    {
        $this->uniqueCode = $uniqueCode;

        return $this;
    }

    public function getTemporary(): ?string
    {
        return $this->temporary;
    }

    public function setTemporary(?string $temporary): self
    {
        $this->temporary = $temporary;

        return $this;
    }
}
