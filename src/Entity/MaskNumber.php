<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rates.
 *
 * @ORM\Table(name="mask_numbers")
 * @ORM\Entity(repositoryClass="App\Repository\MaskNumberRepository")
 */
class MaskNumber
{
    /**
     * @ORM\Id
     * @ORM\Column(name="number", type="string", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $number;

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): MaskNumber
    {
        $this->number = $number;

        return $this;
    }
}
