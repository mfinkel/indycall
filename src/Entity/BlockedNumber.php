<?php

namespace App\Entity;

use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * BlockedNumbers.
 *
 * @ORM\Table(
 *     name="blocked_numbers",
 *     indexes={
 *          @ORM\Index(name="pub_id_idx", columns={"bn_pub_id"}),
 *          @ORM\Index(name="date_exp_idx", columns={"bn_date_exp"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\BlockedNumberRepository")
 */
class BlockedNumber
{
    public const BLOCKED_NUMBER_LIST = 'blockedNumber:List:';
    public const BLOCKED_NUMBER_AVAILABLE_COUNT = 'blockedNumber:AvailableCount:';
    public const BLOCKED_NUMBER_PURCHASE_COUNT = 'blockedNumber:PurchaseCount:';

    /**
     * @ORM\Column(name="bn_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="bn_pub_id", type="integer", nullable=false)
     */
    private int $publisherId;

    /**
     * @ORM\Column(name="bn_phone", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private ?string $blockedNumber = null;

    /**
     * @var DateTimeInterface|Carbon|null
     *
     * @ORM\Column(name="bn_date_bgn", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private DateTimeInterface | Carbon | null $createdAt;

    /**
     * @var DateTimeInterface|Carbon
     *
     * @ORM\Column(name="bn_date_exp", type="datetime", nullable=false)
     */
    private DateTimeInterface | Carbon $dateExpired;

    /**
     * BlockedNumber constructor.
     */
    public function __construct()
    {
        $this->createdAt = Carbon::now();
    }

    public function __toString(): string
    {
        return $this->blockedNumber;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublisherId(): ?int
    {
        return $this->publisherId;
    }

    public function setPublisherId(int $publisherId): self
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    public function getBlockedNumber(): ?string
    {
        return $this->blockedNumber;
    }

    public function setBlockedNumber(?string $blockedNumber): self
    {
        $this->blockedNumber = $blockedNumber;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDateExpired(): DateTimeInterface | Carbon | null
    {
        return $this->dateExpired;
    }

    public function setDateExpired(DateTimeInterface $dateExpired): self
    {
        $this->dateExpired = $dateExpired;

        return $this;
    }
}
