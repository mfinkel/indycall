<?php

namespace App\Entity;

use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Rates.
 *
 * @ORM\Table(name="rates")
 * @ORM\Entity(repositoryClass="App\Repository\RatesRepository")
 */
class Rates
{
    /**
     * @ORM\Column(name="code_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="code", type="string", length=16, nullable=false)
     */
    private string $code;

    /**
     * @ORM\Column(name="codename", type="string", length=128, nullable=false)
     */
    private string $codeName;

    /**
     * @ORM\Column(name="rate", type="float", precision=6, scale=4, nullable=false)
     */
    private float $rate;

    /**
     * @var DateTimeInterface|Carbon
     *
     * @ORM\Column(name="rate_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private DateTimeInterface | Carbon $createdAt;

    /**
     * @ORM\Column(name="rate_prefix", type="string", length=6, nullable=true, options={"default"="NULL"})
     */
    private ?string $ratePrefix = null;

    /**
     * Rates constructor.
     */
    public function __construct()
    {
        $this->createdAt = Carbon::now();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCodeName(): ?string
    {
        return $this->codeName;
    }

    /**
     * @return $this
     */
    public function setCodeName(string $codeName): self
    {
        $this->codeName = $codeName;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    /**
     * @return $this
     */
    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * @return DateTimeInterface|Carbon|null
     */
    public function getCreatedAt(): DateTimeInterface | Carbon | null
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface|Carbon $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTimeInterface | Carbon $createdAt): Rates
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getRatePrefix(): ?string
    {
        return $this->ratePrefix;
    }

    /**
     * @return $this
     */
    public function setRatePrefix(?string $ratePrefix): self
    {
        $this->ratePrefix = $ratePrefix;

        return $this;
    }
}
