<?php

namespace App\Entity\CDR;

class CdrXmlModel
{
    private array $channel_data;
    private array $variables;
    private array $callflow;

    /**
     * @return array
     */
    public function getChannelData(): array
    {
        return $this->channel_data;
    }

    /**
     * @param array $channel_data
     *
     * @return CdrXmlModel
     */
    public function setChannelData(array $channel_data): CdrXmlModel
    {
        $this->channel_data = $channel_data;

        return $this;
    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     *
     * @return CdrXmlModel
     */
    public function setVariables(array $variables): CdrXmlModel
    {
        $this->variables = $variables;

        return $this;
    }

    /**
     * @return array
     */
    public function getCallflow(): array
    {
        return $this->callflow;
    }

    /**
     * @param array $callflow
     *
     * @return CdrXmlModel
     */
    public function setCallflow(array $callflow): CdrXmlModel
    {
        $this->callflow = $callflow;

        return $this;
    }
}
