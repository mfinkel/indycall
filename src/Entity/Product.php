<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products.
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    public const PRODUCT_TYPE_ENABLED = 'Y';

    public const PRODUCT_TYPE_DISABLED = 'N';

    public const PRODUCT_TYPE_1 = 1;

    public const PRODUCT_TYPE_2 = 2;

    public const PRODUCT_TYPE_3 = 3;

    /**
     * @ORM\Column(name="prod_id", type="string", length=45, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private string $id;

    /**
     * @ORM\Column(name="prod_value", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private ?string $productValue = null;

    /**
     * @ORM\Column(name="prod_name", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private ?string $productName = null;

    /**
     * @ORM\Column(name="prod_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private string $productAmount;

    /**
     * @ORM\Column(
     *     name="prod_amount_to_enroll",
     *     type="decimal",
     *     precision=10,
     *     scale=2,
     *     nullable=true,
     *     options={"default"="0"}
     * )
     */
    private ?string $prodAmountToEnroll = null;

    /**
     * @ORM\Column(name="prod_currency", type="string", length=15, nullable=true, options={"default"="NULL"})
     */
    private ?string $productCurrency = null;

    /**
     * @ORM\Column(name="prod_enabled", type="string", length=0, nullable=false)
     */
    private string $isEnabled = self::PRODUCT_TYPE_DISABLED;

    /**
     * @ORM\Column(name="prod_order", type="boolean", nullable=true)
     */
    private ?bool $productOrder = null;

    /**
     * @ORM\Column(name="prod_type", type="string", length=0, nullable=true, options={"default"="NULL"})
     */
    private ?string $productType = null;

    /**
     * @ORM\Column(name="prod_comments", type="string", length=145, nullable=true, options={"default"="NULL"})
     */
    private ?string $productComments = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getProductValue(): ?string
    {
        return $this->productValue;
    }

    public function setProductValue(?string $productValue): self
    {
        $this->productValue = $productValue;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(?string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getProductAmount(): ?string
    {
        return $this->productAmount;
    }

    public function setProductAmount(string $productAmount): self
    {
        $this->productAmount = $productAmount;

        return $this;
    }

    public function getProdAmountToEnroll(): ?string
    {
        return $this->prodAmountToEnroll;
    }

    public function setProdAmountToEnroll(?string $prodAmountToEnroll): self
    {
        $this->prodAmountToEnroll = $prodAmountToEnroll;

        return $this;
    }

    public function getProductCurrency(): ?string
    {
        return $this->productCurrency;
    }

    public function setProductCurrency(?string $productCurrency): self
    {
        $this->productCurrency = $productCurrency;

        return $this;
    }

    public function getIsEnabled(): ?string
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(string $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getProductOrder(): ?bool
    {
        return $this->productOrder;
    }

    public function setProductOrder(?bool $productOrder): self
    {
        $this->productOrder = $productOrder;

        return $this;
    }

    public function getProductType(): ?string
    {
        return $this->productType;
    }

    public function setProductType(?string $productType): self
    {
        $this->productType = $productType;

        return $this;
    }

    public function getProductComments(): ?string
    {
        return $this->productComments;
    }

    public function setProductComments(?string $productComments): self
    {
        $this->productComments = $productComments;

        return $this;
    }
}
