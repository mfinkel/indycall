<?php

namespace App\Entity;

use App\Interfaces\PaymentsTraitsInterface;
use App\Traits\PaymentsTraits;
use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentsAndroid.
 *
 * @ORM\Table(
 *     name="payments_android",
 *     indexes={
 *          @ORM\Index(name="product_idx", columns={"poa_product_id", "poa_state"}),
 *          @ORM\Index(name="porder_id", columns={"poa_order_id"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PaymentsAndroidRepository")
 */
class PaymentsAndroid implements PaymentsTraitsInterface
{
    use PaymentsTraits;

    /**
     * @ORM\Column(name="poa_signature", type="blob", nullable=false)
     */
    private string $poaSignature;

    public function getPoaSignature(): string
    {
        return $this->poaSignature;
    }

    /**
     * @param $poaSignature
     *
     * @return $this
     */
    public function setPoaSignature($poaSignature): self
    {
        $this->poaSignature = $poaSignature;

        return $this;
    }
}
