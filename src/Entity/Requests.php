<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Requests.
 *
 * @ORM\Table(
 *     name="requests",
 *     indexes={
 *          @ORM\Index(name="login_state_idx", columns={"req_voip_login", "req_state"})
 *     }
 * )
 * @ORM\Entity
 */
class Requests
{
    public const REQUEST_TYPE_NEW = 'NEW';

    public const REQUEST_TYPE_CDR = 'CDR';

    public const REQUEST_TYPE_UPD = 'UPD';

    public const REQUEST_TYPE_DON = 'DON';

    public const REQUEST_TYPE_PRO = 'PRO';

    /**
     * @ORM\Column(name="req_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @ORM\Column(name="req_pub", type="integer", nullable=false)
     */
    private ?int $publisherId;

    /**
     * @ORM\Column(name="req_date", type="datetime", nullable=false)
     */
    private ?DateTimeInterface $reqDate;

    /**
     * @ORM\Column(name="req_voip_login", type="string", length=30, nullable=false)
     */
    private ?string $voipLogin;

    /**
     * @ORM\Column(name="req_voip_pass", type="string", length=10, nullable=false)
     */
    private ?string $voipPass;

    /**
     * @ORM\Column(name="req_voip_ip", type="string", length=20, nullable=false)
     */
    private ?string $voipIp;

    /**
     * @ORM\Column(name="req_pub_ip", type="string", length=50, nullable=false)
     */
    private ?string $publicIp;

    /**
     * @ORM\Column(name="req_dst_number", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private ?string $dstNumber = null;

    /**
     * @ORM\Column(name="req_src_number", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private ?string $sourceNumber = null;

    /**
     * @ORM\Column(name="req_state", type="string", length=0, nullable=false, options={"default"="NEW"})
     */
    private string $requestType = self::REQUEST_TYPE_NEW;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublisherId(): ?int
    {
        return $this->publisherId;
    }

    public function setPublisherId(int $publisherId): self
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    public function getReqDate(): ?DateTimeInterface
    {
        return $this->reqDate;
    }

    public function setReqDate(DateTimeInterface $reqDate): self
    {
        $this->reqDate = $reqDate;

        return $this;
    }

    public function getVoipLogin(): ?string
    {
        return $this->voipLogin;
    }

    public function setVoipLogin(string $voipLogin): self
    {
        $this->voipLogin = $voipLogin;

        return $this;
    }

    public function getVoipPass(): ?string
    {
        return $this->voipPass;
    }

    public function setVoipPass(string $voipPass): self
    {
        $this->voipPass = $voipPass;

        return $this;
    }

    public function getVoipIp(): ?string
    {
        return $this->voipIp;
    }

    public function setVoipIp(string $voipIp): self
    {
        $this->voipIp = $voipIp;

        return $this;
    }

    public function getPublicIp(): ?string
    {
        return $this->publicIp;
    }

    public function setPublicIp(string $publicIp): self
    {
        $this->publicIp = $publicIp;

        return $this;
    }

    public function getDstNumber(): ?string
    {
        return $this->dstNumber;
    }

    public function setDstNumber(?string $dstNumber): self
    {
        $this->dstNumber = $dstNumber;

        return $this;
    }

    public function getSourceNumber(): ?string
    {
        return $this->sourceNumber;
    }

    public function setSourceNumber(?string $sourceNumber): self
    {
        $this->sourceNumber = $sourceNumber;

        return $this;
    }

    public function getRequestType(): ?string
    {
        return $this->requestType;
    }

    public function setRequestType(string $requestType): self
    {
        $this->requestType = $requestType;

        return $this;
    }
}
