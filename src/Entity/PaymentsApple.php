<?php

namespace App\Entity;

use App\Interfaces\PaymentsTraitsInterface;
use App\Traits\PaymentsTraits;
use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentsApple.
 *
 * @ORM\Table(
 *     name="payments_apple",
 *     indexes={
 *          @ORM\Index(name="product_idx", columns={"poa_product_id", "poa_state"}),
 *          @ORM\Index(name="porder_id", columns={"poa_order_id"})
 *     }
 * )
 * @ORM\Entity
 */
class PaymentsApple implements PaymentsTraitsInterface
{
    use PaymentsTraits;
}
