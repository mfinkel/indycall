<?php

namespace App\Security;

use DateTimeInterface;
use Symfony\Component\Security\Core\User\UserInterface;

interface IndyCallUserInterface extends UserInterface
{
    public function getId();

    public function getSerialNumber();

    public function getGoogleId();

    public function getGoogleToken();

    public function getGoogleRefreshToken();

    public function getJsonData(): array;

    public function getCreatedAt();

    public function getUpdatedAt();

    public function getUniqueCode();

    public function getTemporary();

    public function setSerialNumber(string $serialNumber): self;

    public function setGoogleId(?string $googleId): self;

    public function setGoogleToken(?string $googleToken): self;

    public function setGoogleRefreshToken(?string $googleRefreshToken): self;

    public function setJsonData(array $jsonData): self;

    public function setCreatedAt(DateTimeInterface $createdAt): self;

    public function setUpdatedAt(DateTimeInterface|null $updatedAt): self;

    public function setUniqueCode(?string $uniqueCode): self;

    public function setTemporary(?string $temporary): self;
}
