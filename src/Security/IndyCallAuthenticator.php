<?php

namespace App\Security;

use App\Controller\HealthStatusController;
use App\Exception\PublisherIssue;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\PreAuthenticationJWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\PreAuthenticationJWTUserTokenInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class IndyCallAuthenticator extends JWTTokenAuthenticator
{
    public function supports(Request $request): bool
    {
        if (in_array(
                $request->getPathInfo(),
                [
                    HealthStatusController::HEALTH_URL,
                    '/api/docs',
                    '/directory',
                    '/prepare_redis_data',
                    '/cdr',
                ],
                true
            )
            || str_contains($request->getPathInfo(), '/loadData/')
        ) {
            return false;
        }

        $requestData = $request->toArray();

        return isset($requestData['params']['SecurityKey']);
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     *
     * @return PreAuthenticationJWTUserToken|PreAuthenticationJWTUserTokenInterface
     */
    public function getCredentials(
        Request $request
    ): PreAuthenticationJWTUserToken|PreAuthenticationJWTUserTokenInterface {
        $requestData = $request->toArray();
        $request->headers->set('Authorization', "Bearer {$requestData['params']['SecurityKey']}");

        return parent::getCredentials($request);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $authException): JsonResponse
    {
        throw new PublisherIssue(PublisherIssue::MESSAGE, PublisherIssue::CODE);
    }
}
