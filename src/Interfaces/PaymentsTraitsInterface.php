<?php

declare(strict_types=1);

namespace App\Interfaces;

use Carbon\Carbon;
use DateTimeInterface;

interface PaymentsTraitsInterface
{
    public function setPoaAmount(string $poaAmount): self;

    public function setPoaCurrency(string $poaCurrency): self;

    public function setPoaPublisherId(int $poaPublisherId): self;

    public function setPoaState(string $poaState): self;

    public function setPoaDateCreate(DateTimeInterface | Carbon $poaDateCreate): self;

    public function setPoaDateUpdate(DateTimeInterface | Carbon | null $poaDateUpdate): self;

    public function setPoaProductId(string $poaProductId): self;

    public function setPoaReceipt(string $poaReceipt): self;

    public function setPoaOrderId(string $poaOrderId): self;

    public function setPoaOrderDesc(?string $poaOrderDesc): self;

    public function getPoaId(): ?int;

    public function getPoaAmount();

    public function getPoaCurrency();

    public function getPoaPublisherId();

    public function getPoaState();

    public function getPoaDateCreate();

    public function getPoaDateUpdate();

    public function getPoaProductId();

    public function getPoaReceipt();

    public function getPoaOrderId();

    public function getPoaOrderDesc();
}
