<?php

namespace App\Utils;

class PhoneUtils
{
    public function preparePhoneNumber(string $number): string
    {
        return preg_replace('/\D+/', '', $number);
    }
}
