<?php

namespace App\Utils;

use Carbon\Carbon;

class DateUtils
{
    public static function stringToDate($date, $format = 'Y-m-d\TH:i:s.uP'):Carbon
    {
        return Carbon::createFromFormat(
            'Y-m-d\TH:i:s.uP',
            $date
        );
    }
}
