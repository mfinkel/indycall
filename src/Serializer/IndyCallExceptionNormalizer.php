<?php

namespace App\Serializer;

use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class IndyCallExceptionNormalizer implements NormalizerInterface
{
    private RequestStack $request;

    public function __construct(RequestStack $request)
    {
        $this->request = $request;
    }

    public function normalize(
        mixed $object,
        string $format = null,
        array $context = [],
    ): array {
        $dataRequest = $this->request->getCurrentRequest()->toArray();

        $response = [
            'error' => [
                'message' => $object->getMessage(),
                'code' => (string) $object->getCode(),
            ],
        ];

        if (isset($dataRequest['id'])) {
            $response['id'] = $dataRequest['id'];
        }

        return $response;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof FlattenException;
    }
}
