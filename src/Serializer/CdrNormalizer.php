<?php

namespace App\Serializer;

use App\Entity\CDR\CdrXmlModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CdrNormalizer
{
    public static function xmlToArray(Request $request): CdrXmlModel
    {
        $encoders = [new XmlEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->deserialize(
            $request->getContent(),
            CdrXmlModel::class,
            'xml'
        );
    }
}
