<?php

namespace App\Exception;

class PublisherIssue extends IndyCallException
{
    public const MESSAGE = 'Publisher not saved';
    public const CODE = -99992;

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
