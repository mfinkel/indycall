<?php

namespace App\Exception;

class PurchaseBeforeAddBlock extends IndyCallException
{
    public const CODE = -99984;
    public const MESSAGE = 'Purchase number before adding new block number';

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
