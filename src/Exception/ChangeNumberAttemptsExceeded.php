<?php

namespace App\Exception;

class ChangeNumberAttemptsExceeded extends IndyCallException
{
    public const MESSAGE = 'The number of attempts to change the number has been exceeded';
    public const CODE = -99951;

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
