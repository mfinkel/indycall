<?php

namespace App\Exception;

use JsonException;

class ProductNotFound extends IndyCallException
{
    public const CODE = -99982;
    public const MESSAGE = 'Publisher not saved';

    /**
     * @throws JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
