<?php

namespace App\Exception;

class NoSipData extends IndyCallException
{
    public const CODE = -99981;
    public const MESSAGE = 'No SIP data';

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
