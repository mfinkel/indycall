<?php

namespace App\Exception;

class IndyCallGeneralException extends IndyCallException
{
    public const MESSAGE = 'Server faced issues';
    public const CODE = -666;

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
