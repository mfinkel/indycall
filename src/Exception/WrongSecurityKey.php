<?php

namespace App\Exception;

class WrongSecurityKey extends IndyCallException
{
    public const CODE = -99993;
    public const MESSAGE = 'Wrong security key';

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
