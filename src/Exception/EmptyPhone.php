<?php

namespace App\Exception;

class EmptyPhone extends IndyCallException
{
    public const MESSAGE = 'The phone is empty';
    public const CODE = -99987;

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
