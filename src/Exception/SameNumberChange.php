<?php

namespace App\Exception;

class SameNumberChange extends IndyCallException
{
    public const CODE = -99952;
    public const MESSAGE = 'Attempt to change number on the same';

    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
