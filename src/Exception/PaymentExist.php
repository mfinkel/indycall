<?php

namespace App\Exception;

class PaymentExist extends IndyCallException
{
    public const CODE = -99983;
    public const MESSAGE = 'Payment already exist';

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
