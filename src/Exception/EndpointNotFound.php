<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class EndpointNotFound extends IndyCallException
{
    public const MESSAGE = 'Endpoint not found';
    public const CODE = Response::HTTP_NOT_FOUND;

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
