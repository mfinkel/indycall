<?php

namespace App\Exception;

class ProductExpired extends IndyCallException
{
    public const CODE = -99950;
    public const MESSAGE = 'Product expired';

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
