<?php

namespace App\Exception;

class DuplicationNumber extends IndyCallException
{
    public const MESSAGE = 'This phone already exists, please add another phone';
    public const CODE = -99986;

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
