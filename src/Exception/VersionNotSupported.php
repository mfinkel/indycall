<?php

namespace App\Exception;

class VersionNotSupported extends IndyCallException
{
    public const CODE = -99910;
    public const MESSAGE = 'Version not supported, please update application';

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
