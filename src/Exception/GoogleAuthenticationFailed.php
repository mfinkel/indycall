<?php

namespace App\Exception;

class GoogleAuthenticationFailed extends IndyCallException
{
    public const MESSAGE = 'Google Authentication failed';
    public const CODE = -99403;

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'error' => [
                    'message' => self::MESSAGE,
                    'code' => self::CODE,
                ],
            ],
            JSON_THROW_ON_ERROR
        );
    }
}
