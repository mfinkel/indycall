<?php

namespace App\Controller;

use App\Services\RedisDataBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends AbstractController
{
    /**
     * @var RedisDataBuilder
     */
    private RedisDataBuilder $redisDataBuilder;

    public function __construct(RedisDataBuilder $redisDataBuilder)
    {
        $this->redisDataBuilder = $redisDataBuilder;
    }

    #[Route('/prepare_redis_data', name: 'prepare_redis_data', methods: ["GET"])]
    public function index(): Response
    {
        $this->redisDataBuilder->prepareRedisData();

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ServiceController.php',
        ]);
    }
}
