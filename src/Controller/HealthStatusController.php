<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HealthStatusController.
 */
class HealthStatusController extends AbstractController
{
    public const STATUS_MESSAGE = ['message' => 'status ok'];
    public const HEALTH_URL = '/healthStatus';

    /**
     * @Route("/healthStatus", name="api_health_status", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->json(
            array_merge(
                self::STATUS_MESSAGE,
                ['env' => $this->getParameter('kernel.environment')]
            )
        );
    }
}
