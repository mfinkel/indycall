<?php

namespace App\Controller;

use App\Serializer\CdrNormalizer;
use App\Services\CallDetailRecordService;
use App\Services\FreeSwitchService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FreeSwitchController extends AbstractController
{

    private FreeSwitchService $switchService;
    private LoggerInterface $logger;
    private CallDetailRecordService $recordService;

    public function __construct(
        FreeSwitchService $switchService,
        LoggerInterface $logger,
        CallDetailRecordService $recordService
    ) {
        $this->switchService = $switchService;
        $this->logger = $logger;
        $this->recordService = $recordService;
    }

    #[Route('/directory', name: 'free_switch_directory', methods: ["POST"])]
    public function directoryAction(
        Request $request
    ): Response {
        $requestData = $request->request->all();
        $this->logger->info('Directory request', $requestData);
        $data = $this->switchService->getDirectoryData(trim($requestData['sip_auth_username']));

        return $this->render(
            'freeswitch/fs_directory.xml.twig',
            array_merge($data, ['domain' => $requestData['domain']])
        );
    }

    #[Route('/cdr', name: 'free_switch_cdr')]
    public function cdrAction(
        Request $request
    ): Response {
        $cdrData = CdrNormalizer::xmlToArray($request->request->all());
        $this->recordService->saveCallInformation($cdrData);
        $this->logger->info('CDR request');

        return $this->json(
            [
                'status' => 'ok',
            ]
        );
    }

    #[Route('/loadData/{login}/{phone}', name: 'free_switch_loadData', methods: ["GET"])]
    public function loadDataAction(
        string $login,
        string $phone
    ): Response {
        $this->logger->info('loadData request', ['login' => $login, 'number' => $phone]);
        $data = $this->switchService->loadDataClean($login, $phone);

        return new Response($data);
    }
}
