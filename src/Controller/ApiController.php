<?php

namespace App\Controller;

use App\Exception\EmptyPhone;
use App\Exception\GoogleAuthenticationFailed;
use App\Exception\PublisherIssue;
use App\Exception\PurchaseBeforeAddBlock;
use App\Services\AuthorizationService;
use App\Services\BlockPhoneService;
use App\Services\PremiumNumberService;
use App\Services\ProductService;
use App\Services\PublisherHelpers;
use App\Services\PurchaseValidationService;
use App\Services\SipDataService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    public const BLOCK_PHONE_ADD = 'blockPhoneAdd';

    public const BLOCK_PHONE_LIST = 'blockPhoneList';

    public const BLOCK_PHONE_DELETE = 'blockPhoneDel';

    public const BLOCK_PHONE_EDIT = 'blockPhoneEdit';

    public const DO_REGISTER = 'doRegister';

    public const DO_CHANGE_PREMIUM_NUMBER = 'doChangePremNumber';

    public const GET_USER_DATA = 'getUserData';

    public const SET_GDPR = 'setGdpr';

    public const GET_PRODUCT_LIST = 'getProductsList';

    public const GET_SIP_DATA_CLEAN = 'getSIPDataClean';

    public const ANDROID_PAYMENT = 'setPaymentOrderAndroid';

    public const APPLE_PAYMENT = 'setPaymentOrderIOS';

    public const GET_BANNER = 'getBanner';

    private BlockPhoneService $blockPhoneService;

    private AuthorizationService $authorizationService;

    private PremiumNumberService $premiumNumber;

    private PublisherHelpers $publisherHelpers;

    private SipDataService $sipDataService;

    private LoggerInterface $logger;

    private ProductService $productService;

    private PurchaseValidationService $purchaseValidationService;

    public function __construct(
        AuthorizationService $authorizationService,
        BlockPhoneService $blockPhoneService,
        PremiumNumberService $premiumNumber,
        PublisherHelpers $publisherHelpers,
        SipDataService $sipDataService,
        ProductService $productService,
        PurchaseValidationService $purchaseValidationService,
        LoggerInterface $logger
    ) {
        $this->authorizationService = $authorizationService;
        $this->blockPhoneService = $blockPhoneService;
        $this->premiumNumber = $premiumNumber;
        $this->publisherHelpers = $publisherHelpers;
        $this->sipDataService = $sipDataService;
        $this->productService = $productService;
        $this->purchaseValidationService = $purchaseValidationService;
        $this->logger = $logger;
    }

    /**
     * @Route("/server.ind.php", name="api", methods="POST")
     *
     * @param Request $request
     *
     * @return Response.
     *
     * @throws EmptyPhone
     * @throws GoogleAuthenticationFailed
     * @throws PublisherIssue
     * @throws PurchaseBeforeAddBlock
     * @throws \JsonException
     */
    public function index(Request $request): Response
    {
        $clientIp = $request->getClientIp();
        $requestData = $request->toArray();
        $securityKey = $requestData['params']['SecurityKey'] ?? false;

        if (self::DO_REGISTER !== $requestData['method'] && !$securityKey) {
            throw new PublisherIssue('Unauthorized request', PublisherIssue::CODE);
        }

        $this->logger->info('New Request', $requestData);

        switch ($requestData['method']) {
            case self::DO_REGISTER:
                $result = $this->authorizationService->authorize($requestData, $clientIp);
                break;
            case self::BLOCK_PHONE_ADD:
                $phone = $requestData['params']['Phone'] ?? false;
                if (!$phone) {
                    throw new EmptyPhone(EmptyPhone::MESSAGE, EmptyPhone::CODE);
                }
                $result['result'] = $this->blockPhoneService->blockPhoneAdd($this->getUser(), $phone);
                break;

            case self::BLOCK_PHONE_LIST:
                $result = $this->blockPhoneService->blockPhoneList($this->getUser());
                break;

            case self::BLOCK_PHONE_DELETE:
                $phone = $requestData['params']['Phone'] ?? false;
                if (!$phone) {
                    throw new EmptyPhone(EmptyPhone::MESSAGE, EmptyPhone::CODE);
                }
                $result['result'] = $this->blockPhoneService->blockPhoneDel($this->getUser(), $phone);
                break;

            case self::BLOCK_PHONE_EDIT:
                $phoneSrc = $requestData['params']['PhoneSrc'] ?? false;
                $phoneDst = $requestData['params']['PhoneDst'] ?? false;

                if (!$phoneSrc || !$phoneDst) {
                    throw new EmptyPhone(EmptyPhone::MESSAGE, EmptyPhone::CODE);
                }

                $blockedNumber = $this->blockPhoneService->blockPhoneEdit($this->getUser(), $phoneSrc, $phoneDst);
                $result = ['result' => [$blockedNumber->getBlockedNumber()]];
                break;

            case self::DO_CHANGE_PREMIUM_NUMBER:
                $number = $requestData['params']['PhoneNumber'] ?? false;
                if (!$number) {
                    throw new EmptyPhone(EmptyPhone::MESSAGE, EmptyPhone::CODE);
                }
                $result = $this->premiumNumber->updateNumber($this->getUser(), $number);

                break;

            case self::GET_USER_DATA:
                $result = $this->publisherHelpers->getPublisherData($this->getUser());
                break;

            case self::SET_GDPR:
                $gdprStatus = $requestData['params']['Agree'] ?? false;
                $result = $this->publisherHelpers->updatePublisherGDPR($this->getUser(), $gdprStatus);
                break;

            case self::GET_PRODUCT_LIST:
                $result['result'] = $this->productService->getProducts($this->getUser());
                break;

            case self::GET_SIP_DATA_CLEAN:
                $result['result'] = $this->sipDataService->getCleanSipData($this->getUser());
                break;

            case self::ANDROID_PAYMENT:
                $result['result'] = $this->purchaseValidationService
                    ->validateGooglePurchase($requestData['params'], $this->getUser());
                break;

            case self::APPLE_PAYMENT:
                $result['result'] = $this->purchaseValidationService
                    ->validateApplePurchase($requestData['params'], $this->getUser());
                break;

            case self::GET_BANNER:
                $result['result'] = [
                    "BannerId" => 0,
                    "TimeOut" => 2000,
                    "BannerUrl" => "https://api.indycall.com/banner",
                    "RequestId" => "267614269-EJM0kz4Ts7-25",
                    "PublisherId" => 23473644,
                    "RedirectUrl" => "",
                    "RedirectUrlText" => "",
                    "Time" => "2021-05-03 12:25:21",
                    "Phone" => "91",
                    "PhoneFrom" => "4915902799815",
                    "purchasedBalace" => 0.9,
                ];
                break;

            default:
                return $this->json(
                    [
                        "jsonrpc" => "2.0",
                        'error' => ['message' => 'Endpoint not found'],
                    ],
                    Response::HTTP_NOT_FOUND
                );
        }

        return $this->json(
            array_merge(
                [
                    'id' => $requestData['id'],
                    'jsonrpc' => '2.0',
                ],
                $result
            )
        );
    }
}
