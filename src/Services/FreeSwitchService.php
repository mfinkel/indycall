<?php

namespace App\Services;

use App\Utils\PhoneUtils;
use JsonException;
use Psr\Log\LoggerInterface;

class FreeSwitchService
{
    private RedisHelper $redisHelper;
    private PhoneUtils $phoneUtils;
    private CallCounterService $callCounterService;
    private LoggerInterface $logger;

    public function __construct(
        RedisHelper $redisHelper,
        PhoneUtils $phoneUtils,
        CallCounterService $callCounterService,
        LoggerInterface $logger
    ) {
        $this->redisHelper = $redisHelper;
        $this->phoneUtils = $phoneUtils;
        $this->callCounterService = $callCounterService;
        $this->logger = $logger;
    }

    /**
     * @throws JsonException
     */
    public function getDirectoryData(string $login)
    {
        $data = $this->redisHelper->get("sip:{$login}");
        $jsonDecode = json_decode($data, true, 512, JSON_THROW_ON_ERROR);
        return $data ?
            $jsonDecode :
            array_merge(
                $jsonDecode,
                [
                    'domain' => 'sip.indycall.com',
                    'request_sip_login' => 'hgu3hg97423hg3h443geg4',
                    'request_sip_pass' => 'fj3802hgfv8u4hv8urhv3hg84hvvr',
                    'outbound_caller_name' => '',
                    'outbound_caller_id' => '',
                    'request_call_seconds' => '',
                    'call_prefix' => '',
                    'deadman' => '',
                ]
            );
    }

    /**
     * @throws JsonException
     */
    public function loadDataClean(string $login, string $number): string
    {
        $distNumber = $this->phoneUtils->preparePhoneNumber($number);
        $isBannedNumber = $this->isPhoneBanned($distNumber);
        $key = "sip:{$login}";
        $sipData = json_decode($this->redisHelper->get($key), true, 512, JSON_THROW_ON_ERROR);
        $seconds = $this->callCounterService->countHowManySecondsForCallToNumber($sipData['publisherId'], $distNumber);

        $this->logger->info("available seconds: {$seconds}");

        $isDeadMan = $isBannedNumber ? "DA" : "NET";

        $setData = [
            'request_sip_login' => $sipData['request_sip_login'],
            'request_sip_pass' => $sipData['request_sip_pass'],
            'request_phone_src' => $sipData['request_phone_src'],
            'request_phone_dst' => $distNumber,
            'request_call_seconds' => $seconds * 60,
            'request_route' => $sipData['request_route'],
            'publisher_purchased_amount' => $sipData['publisher_purchased_amount'],
            'publisher_total_loss' => $sipData['publisher_total_loss'],
            'call_prefix' => $sipData['call_prefix'],
            'deadman' => $isDeadMan,
            'block_id' => $sipData['block_id'],
            'publisherId' => $sipData['publisherId'],
        ];

        $this->redisHelper->set($key, json_encode($setData, JSON_THROW_ON_ERROR));
        $this->redisHelper->set("{$login}_{$number}", $seconds, 30);

        return $isDeadMan;
    }

    private function isPhoneBanned(string $number): bool
    {
        return (bool) $this->redisHelper->get("blk:{$number}");
    }
}
