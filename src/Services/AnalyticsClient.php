<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AnalyticsClient
{
    private HttpClientInterface $client;

    private string $baseUrl;

    public function __construct(HttpClientInterface $client, string $baseUrl)
    {
        $this->client = $client;
        $this->baseUrl = $baseUrl;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendAnalytics(string $userId, string $methodName, string $userIp): void
    {
        $this->client->request(
            Request::METHOD_GET,
            $this->baseUrl,
            [
                'query' => [
                    'v' => 1,
                    't' => 'pageview',
                    'tid' => 'UA-70081921-7',
                    'cid' => $userId,
                    'dp' => $methodName,
                    'uip' => $userIp,
                ],
            ]
        );
    }
}
