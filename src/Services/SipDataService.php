<?php

namespace App\Services;

use App\Entity\Publisher;
use App\Entity\SipLoginData;
use App\Security\IndyCallUserInterface;
use App\Utils\DateUtils;

class SipDataService
{
    public const REQUEST_ROUTE = 'mera1';

    private MaskNumberService $maskNumberService;

    private string $callLowLimitAmount;

    private string $sipPrefixRoutePremium;

    private string $sipPrefixRouteRegular;

    private array $fsServerIp;

    private int $fsServerPort;

    private RedisHelper $redisHelper;

    public function __construct(
        MaskNumberService $maskNumberService,
        RedisHelper $redisHelper,
        string $callLowLimitAmount,
        string $sipPrefixRoutePremium,
        string $sipPrefixRouteRegular,
        array $fsServerIp,
        int $fsServerPort,
        CallCounterService $callCounterService
    ) {
        $this->redisHelper = $redisHelper;
        $this->maskNumberService = $maskNumberService;
        $this->callLowLimitAmount = $callLowLimitAmount;
        $this->sipPrefixRoutePremium = $sipPrefixRoutePremium;
        $this->sipPrefixRouteRegular = $sipPrefixRouteRegular;
        $this->fsServerIp = $fsServerIp;
        $this->fsServerPort = $fsServerPort;
        $this->callCounterService = $callCounterService;
    }

    /**
     * @param Publisher|IndyCallUserInterface $publisher
     *
     * @return array
     * @throws \JsonException
     */
    public function getCleanSipData(Publisher|IndyCallUserInterface $publisher): array
    {
        $userData = $publisher->getJsonData();
        $isBalancePositive = isset($userData["purchasedBalace"]) && $userData["purchasedBalace"] > 0;
        $callPrefix = $isBalancePositive || $userData["calledAmount"] < $this->callLowLimitAmount
            ? $this->sipPrefixRoutePremium
            : $this->sipPrefixRouteRegular;

        $srcNumber = $this->isPremiumExpired($userData)
            ? $this->maskNumberService->getRandomMaskNumber()
            : $userData["prem_phone"]["number"];
        $sipLogin = $this->getUniqueToken();
        $sipPasswd = $this->getUniqueToken();

        $sipLoginData = new SipLoginData();
        $sipLoginData->setPublisherId($publisher->getId());
        $sipLoginData->setRequestSipLogin($sipLogin);
        $sipLoginData->setRequestSipPass($sipPasswd);
        $sipLoginData->setRequestPhoneSrc($srcNumber);
        $sipLoginData->setRequestPhoneDst(null);
        $sipLoginData->setRequestCallSeconds(0);
        $sipLoginData->setRequestRoute(self::REQUEST_ROUTE);
        $sipLoginData->setPublisherPurchasedAmount($userData['purchasedBalace']);
        $sipLoginData->setPublisherTotalLoss(null);
        $sipLoginData->setCallPrefix($callPrefix);
        $sipLoginData->setDeadMan('NET');
        $sipLoginData->setBlockId(null);

        $this->redisHelper->set("sip:{$sipLogin}", (string) $sipLoginData, 30);

        return [
            'PhoneSrc' => $srcNumber,
            'PhoneDst' => null,
            'Seconds' => 0,
            'Route' => $callPrefix,
            'Amount' => $userData['purchasedBalace'],
            'CallPrefix' => $callPrefix,
            'PublisherId' => $publisher->getId(),
            'SipServer' => $this->fsServerIp[array_rand($this->fsServerIp, 1)],
            'SipServerPort' => $this->fsServerPort,
            'SipLogin' => $sipLogin,
            'SipPassword' => $sipPasswd,
            'SipStunServer' => "stun.l.google.com",
            'SipStunServerPort' => "19302",
        ];
    }

    /**
     * @param array $userData
     *
     * @return bool
     */
    private function isPremiumExpired(array $userData): bool
    {
        $dateExpire = $userData["prem_phone"]["date_expire"];
        if(!$dateExpire) {
            return false;
        }
        return DateUtils::stringToDate($dateExpire)->isPast() || empty($userData["prem_phone"]["number"]);
    }

    private function getUniqueToken(): string
    {
        $bytes = random_bytes(20);

        return bin2hex($bytes);
    }
}
