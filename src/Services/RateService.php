<?php

namespace App\Services;

class RateService
{
    private RedisHelper $redisHelper;


    public function __construct(RedisHelper $redisHelper) {
        $this->redisHelper = $redisHelper;
    }

    public function getRate(string $number): float|bool
    {
        $rate = false;
        $numberLength = strlen($number);
        for ($i = 0; $i < $numberLength; $i++) {
            $rate = $this->redisHelper->get("rate:" . substr($number, 0, -$i));
            if ($rate) {
                break;
            }
        }
        return $rate;
    }
}
