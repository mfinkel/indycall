<?php

namespace App\Services;

use App\Entity\MaskNumber;
use App\Entity\Product;
use App\Entity\Rates;
use Doctrine\ORM\EntityManagerInterface;

class RedisDataBuilder
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var RedisHelper
     */
    private RedisHelper $redisHelper;

    public function __construct(EntityManagerInterface $entityManager, RedisHelper $redisHelper)
    {
        $this->entityManager = $entityManager;
        $this->redisHelper = $redisHelper;
    }

    public function prepareRedisData(): void
    {
        $this->loadMaskNumbers();
        $this->loadProducts();
        $this->loadRates();
    }

    private function loadMaskNumbers(): void
    {
        $maskNumbers = $this->entityManager->getRepository(MaskNumber::class)->getMaskNumbers();
        $count = count($maskNumbers);
        for ($i = 0; $i < $count; $i++) {
            $this->redisHelper->set(
                "mask:{$maskNumbers[$i]['number']}",
                $maskNumbers[$i]['number']
            );
        }
    }

    private function loadProducts(): void
    {
        $products = $this->entityManager->getRepository(Product::class)->getAllEnabledProducts();
        $count = count($products);
        for ($i = 0; $i < $count; $i++) {
            $this->redisHelper->set(
                "product:{$products[$i]['productType']}:{$products[$i]['id']}",
                json_encode($products[$i], JSON_THROW_ON_ERROR)
            );
        }
    }

    private function loadRates(): void
    {
        $rates = $this->entityManager->getRepository(Rates::class)->getAllRates();
        $count = count($rates);
        for ($i = 0; $i < $count; $i++) {
            $this->redisHelper->set(
                "rate:{$rates[$i]['code']}",
                $rates[$i]['rate']
            );
        }
    }
}
