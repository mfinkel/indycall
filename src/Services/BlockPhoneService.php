<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\BlockedNumber;
use App\Entity\Publisher;
use App\Exception\EmptyPhone;
use App\Exception\PurchaseBeforeAddBlock;
use App\Security\IndyCallUserInterface;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BlockPhoneService.
 */
class BlockPhoneService
{
    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param IndyCallUserInterface|Publisher $publisher
     * @param string $number
     *
     * @return bool
     *
     * @throws PurchaseBeforeAddBlock
     */
    public function blockPhoneAdd(IndyCallUserInterface|Publisher $publisher, string $number): bool
    {
        $blockedNumbersCount = $this->entityManager
            ->getRepository(BlockedNumber::class)
            ->countAvailableSlots($publisher);

        $availableSlotsCount = $this->entityManager
            ->getRepository(BlockedNumber::class)
            ->countPurchasedSlots($publisher);

        // If not blocked numbers - add it
        if ($blockedNumbersCount && !$availableSlotsCount) {
            throw new PurchaseBeforeAddBlock(PurchaseBeforeAddBlock::MESSAGE, PurchaseBeforeAddBlock::CODE);
        }

        $bnDateExp = !$availableSlotsCount
            ? Carbon::now()->days(30)
            : Carbon::now()->addYears(50);

        $blockedNumber = $this->createBlockNumber($publisher, $number, $blockedNumbersCount);
        $blockedNumber->setDateExpired($bnDateExp);
        $this->entityManager->persist($blockedNumber);
        $this->entityManager->flush();
        $this->invalidateCache($publisher);

        return true;
    }

    /**
     * @param IndyCallUserInterface|Publisher $publisher
     *
     * @return array[]
     */
    public function blockPhoneList(IndyCallUserInterface|Publisher $publisher): array
    {
        $blockedNumberRepository = $this->entityManager->getRepository(BlockedNumber::class);

        $blockedNumberList = $blockedNumberRepository
            ->getBlockedNumberList($publisher);
        $countAvailableSlots = $blockedNumberRepository
            ->countPurchasedSlots($publisher);

        if (!$blockedNumberList) {
            return [
                'result' => [
                    'phones' => [],
                    'total_count' => count($countAvailableSlots),
                    'free_count' => count($countAvailableSlots),
                ]
            ];
        }

        $phoneResult = [];
        foreach ($blockedNumberList as $phone) {
            if ($phone['blockedNumber']) {
                $phoneResult[] = $phone['blockedNumber'];
            }
        }

        return [
            'result' => [
                'phones' => $phoneResult,
                'total_count' => count($countAvailableSlots),
                'free_count' => count($countAvailableSlots),
            ],
        ];
    }

    /**
     * @param IndyCallUserInterface|Publisher $publisher
     * @param string $number
     *
     * @return bool
     */
    public function blockPhoneDel(IndyCallUserInterface|Publisher $publisher, string $number): bool
    {
        /** @var BlockedNumber $blockNumber */
        $blockNumber = $this->entityManager
            ->getRepository(BlockedNumber::class)
            ->findOneBy(
                [
                    'publisherId' => $publisher->getId(),
                    'blockedNumber' => $number,
                ]
            );

        if(!$blockNumber) {
            return true;
        }
        $blockNumber->setBlockedNumber('');
        $this->entityManager->persist($blockNumber);
        $this->entityManager->flush();
        $this->invalidateCache($publisher);

        return true;
    }

    /**
     * @param IndyCallUserInterface|Publisher $publisher
     * @param string $phoneSrc
     * @param string $phoneDst
     *
     * @return BlockedNumber
     *
     * @throws EmptyPhone
     */
    public function blockPhoneEdit(
        IndyCallUserInterface|Publisher $publisher,
        string $phoneSrc,
        string $phoneDst
    ): BlockedNumber {
        /** @var BlockedNumber $phone */
        $phone = $this->entityManager->getRepository(BlockedNumber::class)->findOneBy(
            [
                'publisherId' => $publisher->getId(),
                'blockedNumber' => $phoneSrc,
            ]
        );

        if (!$phone) {
            throw new EmptyPhone(EmptyPhone::MESSAGE, EmptyPhone::CODE);
        }
        $phone->setBlockedNumber($phoneDst);
        $this->entityManager->flush();
        $this->invalidateCache($publisher);

        return $phone;
    }

    /**
     * @param IndyCallUserInterface|Publisher $publisher
     * @param string $number
     * @param array $blockedNumbersCount
     *
     * @return BlockedNumber
     */
    private function createBlockNumber(
        IndyCallUserInterface|Publisher $publisher,
        string $number,
        array $blockedNumbersCount
    ): BlockedNumber {
        if (!$blockedNumbersCount) {
            $blockedNumber = new BlockedNumber();
            $blockedNumber->setPublisherId($publisher->getId());
        } else {
            /** @var BlockedNumber $item */
            foreach ($blockedNumbersCount as $item) {
                if (!$item->getBlockedNumber()) {
                    $blockedNumber = $item;
                    break;
                }
            }
        }
        $blockedNumber->setBlockedNumber($number);

        return $blockedNumber;
    }

    /**
     * @param IndyCallUserInterface|Publisher $publisher
     */
    private function invalidateCache(IndyCallUserInterface|Publisher $publisher): void
    {
        $cacheDriver = $this->entityManager->getConfiguration()->getResultCacheImpl();
        if ($cacheDriver) {
            $cacheDriver->delete(BlockedNumber::BLOCKED_NUMBER_LIST . $publisher->getId());
            $cacheDriver->delete(BlockedNumber::BLOCKED_NUMBER_AVAILABLE_COUNT . $publisher->getId());
            $cacheDriver->delete(BlockedNumber::BLOCKED_NUMBER_PURCHASE_COUNT . $publisher->getId());
        }
    }
}
