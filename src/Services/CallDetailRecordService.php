<?php

namespace App\Services;

use App\Entity\CallDetailRecord;
use App\Entity\CDR\CdrXmlModel;
use App\Entity\Publisher;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CallDetailRecordService
{
    private EntityManagerInterface $entityManager;
    private LoggerInterface $logger;
    private RateService $rateService;
    private PublisherBalanceService $balanceService;

    public function __construct(
        EntityManagerInterface $entityManager,
        RateService $rateService,
        PublisherBalanceService $balanceService,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->rateService = $rateService;
        $this->balanceService = $balanceService;
        $this->logger = $logger;
    }

    public function saveCallInformation(CdrXmlModel $cdrXml)
    {
        $variablesSection = $cdrXml->getVariables();
        $this->logger->info('save CDR Request', $variablesSection);
        $this->publisherCharge($variablesSection);
        $this->storeCdr($variablesSection);
    }

    private function publisherCharge(array $variablesSection): void
    {
        $publisher = $this->entityManager->getRepository(Publisher::class)->find(
            (int) $variablesSection['sip_from_user']
        );
        if (!$publisher) {
            $this->logger->critical("Unable to find Publisher with ID: {$variablesSection['sip_from_user']}");
        }
        $rate = 0;

        if ($variablesSection['billsec'] > 0) {
            $rate = $this->rateService->getRate($variablesSection['sip_to_user']);
        }

        $chargePrice = $variablesSection['billsec'] * $rate / 100;

        if ($chargePrice) {
            $message = "Call on: {$variablesSection['sip_to_user']},";
            $message .= "from: {$variablesSection['effective_caller_id_number']},";
            $message .= "{$variablesSection['billsec']} sec";

            $this->balanceService->updatePublisherBalance(
                $publisher,
                $chargePrice,
                PublisherBalanceService::OUTCOME_CDR,
                $message
            );
        }
    }

    private function storeCdr(array $variablesSection): void
    {
        // those who create a database was so stupid to create id but without autoincrement
        $lastId = (int) $this->entityManager->getRepository(CallDetailRecord::class)->getLastId();
        $cdr = new CallDetailRecord();
        $cdr->setId($lastId ? $lastId + 1 : 1);
        $cdr->setDate(new DateTime());
        $cdr->setPubId($variablesSection['sip_from_user']);
        $cdr->setPubIp($variablesSection['sip_network_ip']);
        $cdr->setCallFrom($variablesSection['sip_req_user']);
        $cdr->setCallTo($variablesSection['sip_to_user']);
        $cdr->setCallDuration($variablesSection['billsec']);
    }
}
