<?php

namespace App\Services;

use App\Entity\Product;
use App\Entity\Publisher;
use App\Entity\PublisherCharge;
use App\Interfaces\PaymentsTraitsInterface;
use App\Security\IndyCallUserInterface;
use App\Utils\DateUtils;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;

class PublisherBalanceService
{
    private EntityManagerInterface $entityManager;

    public const PAYMENT_TYPE_INCOME = 'i';

    public const PAYMENT_TYPE_OUTCOME = 'o';

    public const INCOME_GOOGLE = 'GG';

    public const INCOME_APPLE = 'AP';

    public const OUTCOME_CDR = 'CAL';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Publisher|IndyCallUserInterface $publisher
     * @param PaymentsTraitsInterface $payments
     * @param Product $product
     * @param string $operationRequester
     *
     * @throws \JsonException
     */
    public function updatePublisherWithInAppPurchase(
        Publisher|IndyCallUserInterface $publisher,
        PaymentsTraitsInterface $payments,
        Product $product,
        string $operationRequester
    ): void {
        $publisherData = $publisher->getJsonData();

        $publisherAmount = (float) $publisherData['amount']['balance'];
        $publisherData['amount']['balance'] = $publisherAmount + $product->getProdAmountToEnroll();
        $publisher->setJsonData($publisherData);
        $this->entityManager->persist($publisher);
        $this->createPublisherChargedRecord(
            $publisher,
            $payments->getPoaAmount(),
            $publisherAmount,
            self::PAYMENT_TYPE_INCOME,
            $operationRequester,
            "Publisher balance increase type: i, resource: {$operationRequester} amount: {$payments->getPoaAmount()}"
        );
        $this->entityManager->flush();
    }

    /**
     * @param Publisher|IndyCallUserInterface $publisher
     * @param float $amount
     * @param string $operation
     * @param string $message
     *
     * @throws \JsonException
     */
    public function updatePublisherBalance(
        Publisher|IndyCallUserInterface $publisher,
        float $amount,
        string $operation,
        string $message
    ) {
        $publisherData = $publisher->getJsonData();
        $publisherAmount = (float) $publisherData['amount']['balance'];
        $publisherData['amount']['balance'] = $publisherAmount - $amount;
        $publisher->setJsonData($publisherData);
        $this->createPublisherChargedRecord(
            $publisher,
            $amount,
            $publisherAmount,
            self::PAYMENT_TYPE_INCOME,
            $operation,
            $message
        );
    }

    /**
     * @param Publisher|IndyCallUserInterface $publisher
     * @param float $amount
     * @param float $balanceBefore
     * @param string $operationType
     * @param string $operationRequester
     * @param string $description
     */
    private function createPublisherChargedRecord(
        Publisher|IndyCallUserInterface $publisher,
        float $amount,
        float $balanceBefore,
        string $operationType,
        string $operationRequester,
        string $description
    ): void {
        $charge = new PublisherCharge();
        $charge->setPublisherId($publisher->getId());
        $charge->setPayDate(Carbon::create('now'));
        $charge->setPaymentType($operationType);
        $charge->setPaymentSource($operationRequester);
        $charge->setBalanceBefore($balanceBefore);
        $charge->setAmount($amount);
        $charge->setBalanceAfter($balanceBefore + $amount);
        $charge->setDescription($description);
        $this->entityManager->persist($charge);
        $this->entityManager->flush();
    }

    /**
     * @param Publisher|IndyCallUserInterface $publisher
     *
     * @throws \JsonException
     */
    public function updatePremiumBlockNumber(Publisher|IndyCallUserInterface $publisher): void
    {
        $publisherData = $publisher->getJsonData();
        $premPhoneDateExpired = DateUtils::stringToDate($publisherData['prem_phone']['date_expire']);
        if ($premPhoneDateExpired->isPast()) {
            $publisherData['prem_phone']['date_expire'] = Carbon::create('now')->addDays(3 * 30);
        } else {
            $publisherData['prem_phone']['date_expire'] = $premPhoneDateExpired->addDays(3 * 30);
        }

        $publisherData['prem_phone']['tryis'] += 5;
        $publisher->setJsonData($publisherData);
        $this->entityManager->persist($publisher);
        $this->entityManager->flush();
    }

    /**
     * @param Publisher|IndyCallUserInterface $publisher
     *
     * @throws \JsonException
     */
    public function updateTrialBlockNumber(Publisher|IndyCallUserInterface $publisher): void
    {
        $publisherData = $publisher->getJsonData();
        $publisherData['prem_phone']['date_expire'] = Carbon::create('now')->addDay();
        $publisherData['prem_phone']['tryis'] = 2;
        $publisher->setJsonData($publisherData);
        $this->entityManager->persist($publisher);
        $this->entityManager->flush();
    }
}
