<?php

namespace App\Services;

use App\Entity\BlockedNumber;
use App\Entity\Publisher;
use App\Exception\GoogleAuthenticationFailed;
use App\Exception\PublisherIssue;
use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Psr\Log\LoggerInterface;

/**
 * Class AuthorizationService.
 */
class AuthorizationService
{
    public const USER_DEFAULT_DATA = [
        'lastIp' => '',
        'geo' => [
            'lat' => 0,
            'lon' => 0,
        ],
        'amount' => [
            'balance' => 0,
            'purchase_enabled' => 1,
            'called_amount' => 0,
        ],
        'prem_phone' => [
            'number' => '',
            'date_expire' => null,
            'enabled' => 1,
            'tryis' => 0,
        ],
        'user_area' => [
            'enabled' => 1,
            'uniq_code_enabled' => 1,
        ],
        'gdpr' => [
            'date' => '',
            'status' => 0,
            'gdpr' => 0,
        ],
    ];

    private LoggerInterface $logger;

    private EntityManagerInterface $entityManager;

    private JWTManager $jwtManager;

    /**
     * @var GoogleOauthClient
     */
    private GoogleOauthClient $oauthClient;

    /**
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     * @param JWTManager $JWTManager
     * @param GoogleOauthClient $oauthClient
     */
    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        JWTManager $JWTManager,
        GoogleOauthClient $oauthClient,
    ) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->jwtManager = $JWTManager;
        $this->oauthClient = $oauthClient;
    }

    /**
     * @param array $requestData
     * @param string $clientIp
     *
     * @return array
     *
     * @throws GoogleAuthenticationFailed
     * @throws PublisherIssue
     * @throws \JsonException
     */
    public function authorize(array $requestData, string $clientIp): array
    {
        $params = $requestData['params'];
        $clientID = $params['clientID'] ?? null;
        $authCode = $params['authCode'] ?? null;
        $refreshCode = $params['refreshCode'] ?? null;
        if (!$clientID && !$authCode && !$refreshCode) {
            throw new PublisherIssue(PublisherIssue::MESSAGE, PublisherIssue::CODE);
        }

        if ($clientID) {
            $isUserRegistered = $this->getPublisherByClientId($clientID, $clientIp);
        }

        if ($authCode) {
            $isUserRegistered = $this->getPublisherByGoogleToken($authCode, $clientIp);
        }

        if ($refreshCode) {
            $isUserRegistered = $this->refreshSession($refreshCode);
        }

        if (!$isUserRegistered) {
            throw new PublisherIssue(PublisherIssue::MESSAGE, PublisherIssue::CODE);
        }

        return [
            'id' => $requestData['id'],
            'result' => [
                'SecurityKey' => $this->jwtManager->create($isUserRegistered),
                'RefreshToken' => $isUserRegistered->getGoogleRefreshToken(),
            ],
        ];
    }

    /**
     * @param $field
     * @param $value
     *
     * @return Publisher|null
     */
    private function isUserRegistered($field, $value): Publisher|null
    {
        return $this->entityManager->getRepository(Publisher::class)->findOneBy(
            [$field => $value]
        );
    }

    /**
     * @param string $clientIp
     * @param string|null $clientID
     * @param string|null $gmail
     * @param string|null $token
     * @param string|null $refreshToken
     *
     * @return Publisher
     * @throws \JsonException
     */
    private function createPublisher(
        string $clientIp,
        string $clientID = null,
        string $gmail = null,
        string $token = null,
        string $refreshToken = null
    ): Publisher {
        $userData = array_merge(
            PublisherHelpers::BASE_PUBLISHER_DATA,
            self::USER_DEFAULT_DATA,
            ['lastIp' => $clientIp]
        );
        $this->logger->info('Create new Publisher');
        $publisher = new Publisher();
        $publisher->setSerialNumber($clientID ?: $gmail);
        $publisher->setGoogleId($gmail);
        $publisher->setGoogleToken(json_encode($token, JSON_THROW_ON_ERROR));
        $publisher->setGoogleRefreshToken($refreshToken);
        $publisher->setJsonData($userData);
        $publisher->setCreatedAt(new DateTime());
        $publisher->setUpdatedAt(new DateTime());
        $this->entityManager->persist($publisher);
        $this->entityManager->flush();
        $blockNumber = new BlockedNumber();
        $blockNumber->setDateExpired(Carbon::create('now')->addMonth());
        $blockNumber->setPublisherId($publisher->getId());
        $this->entityManager->persist($blockNumber);
        $this->entityManager->flush();
        $this->logger->info('New Publisher with one block number slot created');

        return $publisher;
    }

    /**
     * @param Publisher $publisher
     */
    private function publisherLastDateUpdate(Publisher $publisher): void
    {
        $publisher->setUpdatedAt(Carbon::create('now'));
        $this->entityManager->persist($publisher);
        $this->entityManager->flush();
    }

    /**
     * @param string $clientID
     * @param string $clientIp
     *
     * @return Publisher
     *
     * @throws \JsonException
     */
    private function getPublisherByClientId(string $clientID, string $clientIp): Publisher
    {
        $this->logger->info('Android 9 and less authorization');

        /** @var Publisher $isUserRegistered */
        $isUserRegistered = $this->isUserRegistered('serialNumber', $clientID);

        if (!$isUserRegistered) {
            $isUserRegistered = $this->createPublisher($clientIp, $clientID);
        } else {
            $this->publisherLastDateUpdate($isUserRegistered);
        }

        return $isUserRegistered;
    }

    /**
     * @param string $authCode
     * @param string $clientIp
     *
     * @return Publisher
     * @throws GoogleAuthenticationFailed
     * @throws \JsonException
     */
    private function getPublisherByGoogleToken(string $authCode, string $clientIp): Publisher
    {
        $this->logger->info("Google Auth Code authorization {$authCode}");
        $googleUserData = $this->oauthClient->exchangeCodeToAccessToken($authCode);
        $this->logger->info("Google Auth exchange code: " . json_encode($googleUserData, JSON_THROW_ON_ERROR));
        $isUserRegistered = $this->isUserRegistered('googleId', $googleUserData['email']);
        $this->logger->info("Google Auth new user?: " . (bool)$isUserRegistered);
        if (!$isUserRegistered) {
            $this->logger->info('New Publisher with Google Auth Token');
            $isUserRegistered = $this->createPublisher(
                $clientIp,
                null,
                $googleUserData['email'],
                $googleUserData['accessToken'],
                $googleUserData['refreshToken']
            );
            $this->logger->info("Google Auth add new user");
        } else {
            $this->logger->info("Google Auth update last date");
            $this->publisherLastDateUpdate($isUserRegistered);
        }

        return $isUserRegistered;
    }

    /**
     * @param string $googleToken
     *
     * @return Publisher
     *
     * @throws PublisherIssue
     */
    private function refreshSession(string $googleToken): Publisher
    {
        /** @var Publisher $publisher */
        $publisher = $this->entityManager
            ->getRepository(Publisher::class)
            ->findPublisherByToken($googleToken);

        if (!$publisher) {
            throw new PublisherIssue(PublisherIssue::MESSAGE, PublisherIssue::CODE);
        }

        $googleUser = $this->oauthClient->exchangeRefreshToken($publisher->getGoogleRefreshToken());

        if (!$googleUser) {
            throw new PublisherIssue(PublisherIssue::MESSAGE, PublisherIssue::CODE);
        }

        $this->publisherLastDateUpdate($publisher);

        return $publisher;
    }
}
