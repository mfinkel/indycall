<?php

namespace App\Services;

use App\Entity\Publisher;
use App\Entity\PurchaseHistory;
use App\Exception\ChangeNumberAttemptsExceeded;
use App\Exception\ProductExpired;
use App\Exception\SameNumberChange;
use App\Security\IndyCallUserInterface;
use App\Utils\PhoneUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;

class PremiumNumberService
{
    /**
     * @var PhoneUtils
     */
    private PhoneUtils $utils;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PhoneUtils $utils
     */
    public function __construct(EntityManagerInterface $entityManager, PhoneUtils $utils)
    {
        $this->utils = $utils;
        $this->entityManager = $entityManager;
    }

    /**
     * @param IndyCallUserInterface|Publisher $user
     * @param string $number
     *
     * @return array
     *
     * @throws ChangeNumberAttemptsExceeded
     * @throws JsonException
     * @throws ProductExpired
     * @throws SameNumberChange
     */
    public function updateNumber(IndyCallUserInterface|Publisher $user, string $number): array
    {
        /** @var IndyCallUserInterface $user */
        $publisherJsonData = $user->getJsonData();
        $number = $this->utils->preparePhoneNumber($number);

        if ($publisherJsonData['prem_phone']['tryis'] < 0) {
            throw new ChangeNumberAttemptsExceeded(
                ChangeNumberAttemptsExceeded::MESSAGE,
                ChangeNumberAttemptsExceeded::CODE
            );
        }

        $now = new DateTime();
        $expiredAt = new DateTime($publisherJsonData['prem_phone']['date_expire']);
        if ($now > $expiredAt) {
            throw new ProductExpired(ProductExpired::MESSAGE, ProductExpired::CODE);
        }

        if ($number === $publisherJsonData['prem_phone']['number']) {
            throw new SameNumberChange(SameNumberChange::MESSAGE, SameNumberChange::CODE);
        }

        $previousPremNumber = $publisherJsonData['prem_phone']['number'];
        $publisherJsonData['prem_phone']['tryis'] -= 1;
        $publisherJsonData['prem_phone']['number'] = $number;

        $this->purchase($user, $previousPremNumber, $number, $publisherJsonData['prem_phone']['tryis']);

        $this->entityManager->persist($user->setJsonData($publisherJsonData));
        $this->entityManager->flush();

        return [
            'result' => [
                'changeCount' => $publisherJsonData['prem_phone']['tryis'],
                'dateValid' => $publisherJsonData['prem_phone']['date_expire'],
                'number' => $number,
            ],
        ];
    }

    /**
     * @param IndyCallUserInterface|Publisher $user
     * @param string $previousPremNumber
     * @param string $number
     * @param $triesLeft
     */
    private function purchase(
        IndyCallUserInterface|Publisher $user,
        string $previousPremNumber,
        string $number,
        $triesLeft
    ): void {
        /** @var Publisher $user */
        $purchase = new PurchaseHistory();
        $purchase->setPublisherId($user->getId());
        $purchase->setNumberBefore($previousPremNumber);
        $purchase->setNewNumber($number);
        $purchase->setAttemptsLeft($triesLeft);
        $this->entityManager->persist($purchase);
        $this->entityManager->flush();
    }
}
