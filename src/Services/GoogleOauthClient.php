<?php

namespace App\Services;

use App\Exception\GoogleAuthenticationFailed;
use Exception;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\Log\LoggerInterface;

class GoogleOauthClient
{
    private ClientRegistry $clientRegistry;

    private LoggerInterface $logger;
    private string $redirectUrl;

    public function __construct(ClientRegistry $clientRegistry, LoggerInterface $logger, string $redirectUrl)
    {
        $this->clientRegistry = $clientRegistry;
        $this->logger = $logger;
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return OAuth2ClientInterface
     */
    private function getClient(): OAuth2ClientInterface
    {
        return $this->clientRegistry->getClient('google_main');
    }

    /**
     * @param string $authCode
     *
     * @return AccessTokenInterface|AccessToken
     *
     * @throws IdentityProviderException
     */
    private function getAccessToken(string $authCode): AccessTokenInterface|AccessToken
    {
        $auth2Client = $this->getClient();

        return $auth2Client->getOAuth2Provider()->getAccessToken(
            'authorization_code',
            [
                'code' => $authCode,
                'scope' => 'email openid',
                'access_type' => 'offline',
                'prompt' => 'consent',
                'redirect_uri' => $this->redirectUrl
            ]
        );
    }

    /**
     * @param AccessTokenInterface|AccessToken $accessToken
     *
     * @return ResourceOwnerInterface
     */
    private function fetchUserFromToken(AccessTokenInterface|AccessToken $accessToken): ResourceOwnerInterface
    {
        $auth2Client = $this->getClient();

        return $auth2Client->fetchUserFromToken($accessToken);
    }

    /**
     * @param string $authCode
     *
     * @return array
     *
     * @throws GoogleAuthenticationFailed
     * @throws \JsonException
     */
    public function exchangeCodeToAccessToken(string $authCode): array
    {
        try {
            /** @var AccessToken $token */
            $token = $this->getAccessToken(stripslashes($authCode));
            /** @var GoogleUser $googleUser */
            $googleUser = $this->fetchUserFromToken($token);
            $accessToken = $token->getValues()['id_token'];
            $refreshToken = $token->getRefreshToken();
            $userData = $googleUser->toArray();
            $userData['accessToken'] = $token->getToken();
            $userData['token'] = $accessToken;
            $userData['refreshToken'] = $refreshToken;

            return $userData;
        } catch (Exception $e) {
            $this->logger->critical(
                json_encode(
                    [
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                    ],
                    JSON_THROW_ON_ERROR
                )
            );
            throw new GoogleAuthenticationFailed(
                GoogleAuthenticationFailed::MESSAGE,
                GoogleAuthenticationFailed::CODE
            );
        }
    }

    /**
     * @param string $refreshToken
     *
     * @return array
     */
    public function exchangeRefreshToken(string $refreshToken): array
    {
        $auth2Client = $this->getClient();
        $token = $auth2Client->refreshAccessToken(
            $refreshToken,
            [
                'scope' => 'email openid',
                'access_type' => 'offline',
                'prompt' => 'consent',
                'redirect_uri' => $this->redirectUrl
            ]
        );

        $googleUser = $this->fetchUserFromToken($token);
        $accessToken = $token->getValues()['id_token'];
        $refreshToken = $token->getRefreshToken();
        $userData = $googleUser->toArray();
        $userData['accessToken'] = $token->getToken();
        $userData['token'] = $accessToken;
        $userData['refreshToken'] = $refreshToken;

        return $userData;
    }
}
