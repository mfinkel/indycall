<?php

namespace App\Services;

use App\Entity\PaymentsAndroid;
use App\Entity\Product;
use App\Entity\Publisher;
use App\Interfaces\PaymentsTraitsInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class PaymentService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
     {
         $this->entityManager = $entityManager;
     }

    /**
     * @param array $recipe
     * @param Publisher|UserInterface $publisher
     *
     * @throws \JsonException
     */
    public function storePurchase(array $recipe, Publisher|UserInterface $publisher, Product $product, string $signature): PaymentsTraitsInterface
    {
        $payment = new PaymentsAndroid();
        $payment->setPoaAmount($product->getProductAmount());
        $payment->setPoaCurrency('USD');
        $payment->setPoaPublisherId($publisher->getId());
        $payment->setPoaState('D');
        $payment->setPoaProductId($product->getId());
        $payment->setPoaReceipt(json_encode($recipe, JSON_THROW_ON_ERROR));
        $payment->setPoaSignature($signature);
        $payment->setPoaOrderId($recipe['orderId'] ?? $recipe['in_app'][0]['transaction_id']);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        return $payment;
    }
}
