<?php

namespace App\Services;

use App\Entity\BlockedNumber;
use App\Entity\PaymentsAndroid;
use App\Entity\PaymentsApple;
use App\Entity\Product;
use App\Entity\Publisher;
use App\Security\IndyCallUserInterface;
use App\Services\PurchaseValidator\ApplePurchaseValidator;
use App\Services\PurchaseValidator\GooglePurchaseValidator;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Google\Exception;
use Psr\Log\LoggerInterface;

class PurchaseValidationService
{
    public const TEST_TOKEN = 'TEST_API_INDY_TOKEN';
    public const NUMBER_TRY = 'GPA.NUMBER.TRY';


    private GooglePurchaseValidator $googlePurchaseValidator;

    private ApplePurchaseValidator $applePurchaseValidator;

    private PaymentService $paymentService;

    private EntityManagerInterface $entityManager;

    private PublisherBalanceService $balanceService;

    private LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        PaymentService $paymentService,
        GooglePurchaseValidator $googlePurchaseValidator,
        ApplePurchaseValidator $applePurchaseValidator,
        PublisherBalanceService $balanceService,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->paymentService = $paymentService;
        $this->googlePurchaseValidator = $googlePurchaseValidator;
        $this->applePurchaseValidator = $applePurchaseValidator;
        $this->balanceService = $balanceService;
        $this->logger = $logger;
    }

    /**
     * @param array $purchaseData
     * @param Publisher|IndyCallUserInterface $publisher
     *
     * @return array
     * @throws \JsonException
     * @throws Exception
     */
    public function validateGooglePurchase(array $purchaseData, Publisher|IndyCallUserInterface $publisher): array
    {
        $androidPaymentRepository = $this->entityManager->getRepository(PaymentsAndroid::class);
        $recipe = json_decode($purchaseData['Receipt'], true, 512, JSON_THROW_ON_ERROR);

        if($androidPaymentRepository->isOrderExist($recipe['orderId'])){
            return ['Status' => true];
        }

        $signature = $purchaseData['Signature'];
        $isPurchaseValid = $this->googlePurchaseValidator->validate($recipe, $publisher, $signature);

        if (!$isPurchaseValid) {
            $this->logger->warning(
                "Purchase not valid for android, pls check details",
                $purchaseData
            );
            return ['Status' => false];
        }

        $product = $this->getPurchasedProduct($recipe['productId']);

        if(!$product) {
            $this->logger->warning(
                "Unable to find product",
                $purchaseData
            );
            return ['Status' => false];
        }

        if ($recipe['purchaseToken'] === self::TEST_TOKEN) {
            $androidPaymentRepository->removeTestPurchase($recipe['orderId']);
        }

        if($recipe['orderId'] === self::NUMBER_TRY) {
            $this->paymentService->storePurchase($recipe, $publisher, $product, $signature);
            $this->balanceService->updateTrialBlockNumber($publisher);
            return ['Status' => true];
        }

        if ((int)$recipe['purchaseState'] === 0) {
            return $this->storePurchase($recipe, $publisher, $product, $signature, PublisherBalanceService::INCOME_GOOGLE);
        }


        return ['Status' => false];
    }

    /**
     * @param array $purchaseData
     * @param Publisher|IndyCallUserInterface $publisher
     *
     * @return array
     * @throws \JsonException
     */
    public function validateApplePurchase(array $purchaseData, Publisher|IndyCallUserInterface $publisher): array
    {
        $recipe = $purchaseData['Receipt'];
        $fullRecipe = $this->applePurchaseValidator->validate($recipe, $publisher);

        if (!$fullRecipe) {
            $this->logger->warning(
                "Purchase not valid for apple, pls check details",
                $purchaseData
            );
            return ['Status' => false];
        }

        $isOrderExist = $this->entityManager->getRepository(PaymentsApple::class)->findOneBy(
            ['poaOrderId' => $fullRecipe['in_app'][0]['transaction_id']]
        );

        if($isOrderExist){
            return ['Status' => true];
        }

        $product = $this->getPurchasedProduct($fullRecipe['in_app'][0]['product_id']);

        if(!$product) {
            $this->logger->warning(
                "Unable to find product",
                $purchaseData
            );
            return ['Status' => false];
        }

        return $this->storePurchase($fullRecipe, $publisher, $product, '', PublisherBalanceService::INCOME_APPLE);
    }

    /**
     * @param string $productId
     *
     * @return Product|null
     */
    private function getPurchasedProduct(string $productId): ?Product
    {
        $productRepository = $this->entityManager->getRepository(Product::class);
        return $productRepository->getProductById($productId);
    }

    /**
     * @param mixed $recipe
     * @param IndyCallUserInterface|Publisher $publisher
     * @param Product $product
     * @param mixed $signature
     * @return bool[]
     * @throws \JsonException
     */
    private function storePurchase(mixed $recipe, IndyCallUserInterface|Publisher $publisher, Product $product, mixed $signature, string $incomeType): array
    {
        $payment = $this->paymentService->storePurchase($recipe, $publisher, $product, $signature);
        $this->logger->info("Store purchase information");
        $this->balanceService->updatePublisherWithInAppPurchase(
            $publisher,
            $payment,
            $product,
            $incomeType
        );
        $this->logger->info("Update publisher balance");

        if ((int)$product->getProductType() === 3) {
            $this->balanceService->updatePremiumBlockNumber($publisher);
            $this->logger->info("Update publisher with new prem_block_num");
        }

        if ($product->getId() === 'indy.blk.num') {
            $blockNumber = new BlockedNumber();
            $blockNumber->setDateExpired(Carbon::create('now')->addDays(30));
            $blockNumber->setPublisherId($publisher->getId());
            $this->entityManager->persist($blockNumber);
            $this->entityManager->flush();
            $this->logger->info('Update publisher with new block number');
        }

        $this->entityManager->flush();
        return ['Status' => true];
    }
}
