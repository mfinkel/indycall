<?php

namespace App\Services;

use App\Entity\Product;
use App\Entity\Publisher;
use App\Repository\ProductRepository;
use App\Security\IndyCallUserInterface;
use Doctrine\ORM\EntityManagerInterface;

class ProductService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return Product[]
     */
    public function getProducts(Publisher|IndyCallUserInterface $publisher): array
    {
        $products = [];
        /** @var ProductRepository $manager */
        $productRepository = $this->entityManager->getRepository(Product::class);
        $productsList = $productRepository->getAllEnabledProducts();
        $products = $this->buildProductList($productsList, $products);
        $publisherData = $publisher->getJsonData();
        $callerNumberId = 'indy.caller.num';
        if(isset($publisherData['premiumNumber']['dateValid']) && $publisherData['premiumNumber']['dateValid'] !== null) {
            $callerNumberId = 'indy.caller.num.renew';
        }
        $callerNumber = $productRepository->getRenewNumber($callerNumberId);
        $callerNumber[0]['productAmount'] = $callerNumberId === 'indy.caller.num' ? "Check it!" : $callerNumber[0]['productAmount'];
        $callerNumber[0]['productCurrency'] = $callerNumberId === 'indy.caller.num' ? "" : $callerNumber[0]['productCurrency'];
        return $this->buildProductList(
            $callerNumber,
            $products
        );
    }

    /**
     * @param array $productsList
     * @param array $products
     *
     * @return array
     */
    private function buildProductList(array $productsList, array $products): array
    {
        foreach ($productsList as $product) {
            $products[] = [
                'id' => $product['id'],
                'name' => $product['productName'],
                'value' => $product['productValue'],
                'price' => $product['productAmount'],
                'currency' => $product['productCurrency'],
                'order' => (string) $product['productOrder'],
                'type' => $product['productType'],
            ];
        }

        return $products;
}
}
