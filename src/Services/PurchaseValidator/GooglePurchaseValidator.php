<?php

namespace App\Services\PurchaseValidator;

use App\Entity\Publisher;
use App\Services\PurchaseValidationService;
use Google\Client;
use Google\Exception;
use Psr\Log\LoggerInterface;
use ReceiptValidator\GooglePlay\Validator;
use Symfony\Component\Security\Core\User\UserInterface;

class GooglePurchaseValidator implements PurchaseValidatorInterface
{
    private array $inAppJson;

    private string $purchaseUrl;

    private LoggerInterface $logger;

    private string $googleAppName;

    public function __construct(LoggerInterface $logger, array $inAppJson, string $purchaseUrl, string $googleAppName)
    {
        $this->logger = $logger;
        $this->inAppJson = $inAppJson;
        $this->purchaseUrl = $purchaseUrl;
        $this->googleAppName = $googleAppName;
    }

    /**
     * @param array $recipe
     * @param string $signature
     * @param Publisher|UserInterface $publisher
     *
     * @return bool
     * @throws Exception
     */
    public function validate(array|string $recipe, Publisher|UserInterface $publisher, string $signature = ''): bool|array
    {
        if ($recipe['orderId'] === PurchaseValidationService::NUMBER_TRY) {
            return true;
        }

        $client = new Client();
        $client->setApplicationName($this->googleAppName);
        $client->setAuthConfig($this->inAppJson);
        $client->setScopes($this->purchaseUrl);

        $validator = new Validator(new \Google_Service_AndroidPublisher($client));

        try {
            $response = $validator->setPackageName($recipe['packageName'])
                ->setProductId($recipe['productId'])
                ->setPurchaseToken($recipe['purchaseToken'])
                ->validatePurchase();
        } catch (\Exception $e) {
            $this->logger->warning('Validation of purchase failed', $recipe);

            return false;
        }
        $publisherProductPurchase = $response->getRawResponse();

        return $recipe['purchaseState'] === $publisherProductPurchase->getPurchaseState()
            && $recipe['orderId'] === $publisherProductPurchase->getOrderId()
            && (int)$recipe['purchaseTime'] === (int)$publisherProductPurchase->getPurchaseTimeMillis();
    }
}
