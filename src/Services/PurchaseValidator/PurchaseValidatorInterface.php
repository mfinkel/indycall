<?php

namespace App\Services\PurchaseValidator;

use App\Entity\Publisher;
use App\Security\IndyCallUserInterface;

interface PurchaseValidatorInterface
{
    public function validate(array|string $recipe, Publisher|IndyCallUserInterface $publisher, string $signature = ''): bool|array;
}
