<?php

namespace App\Services\PurchaseValidator;

use App\Entity\Publisher;
use App\Security\IndyCallUserInterface;
use App\Services\PurchaseValidationService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApplePurchaseValidator implements PurchaseValidatorInterface
{
    private string $purchaseUrl;

    private LoggerInterface $logger;

    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $client, LoggerInterface $logger, string $purchaseUrl)
    {
        $this->httpClient = $client;
        $this->logger = $logger;
        $this->purchaseUrl = $purchaseUrl;
    }

    /**
     * @param array $recipe
     * @param Publisher|IndyCallUserInterface $publisher
     * @param string $signature
     *
     * @return bool
     */
    public function validate(array|string $recipe, Publisher|IndyCallUserInterface $publisher, string $signature = ''): bool|array
    {
        if(is_array($recipe)) {
            throw new \Exception('Wrong recipe format, expect to be string, array given');
        }

        if ($recipe === PurchaseValidationService::NUMBER_TRY) {
            return true;
        }

        try {
            $response = $this->httpClient->request(
                Request::METHOD_POST,
                $this->purchaseUrl,
                [
                    'body' => json_encode(['receipt-data' => $recipe], JSON_THROW_ON_ERROR),
                    'headers' => ['content-type' => 'application/json']
                ]
            );
            $decodedResponse = $response->toArray();

            if($decodedResponse['status'] !== 0) {
                $this->logger->warning("Validation of purchase failed: status different from expected", [$recipe]);
                return false;
            }
        } catch (TransportExceptionInterface $e) {
            $this->logger->warning("Validation of purchase failed: {$e->getMessage()}", [$recipe]);
            return false;
        }

        if ($decodedResponse['receipt']['in_app'][0]['product_id'] === 'indy.caller.number.renew'){
            $decodedResponse['receipt']['in_app'][0]['product_id'] = 'indy.caller.num.renew';
        }

        return $decodedResponse['receipt'];
    }
}
