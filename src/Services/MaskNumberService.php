<?php

namespace App\Services;

use App\Entity\MaskNumber;
use Doctrine\ORM\EntityManagerInterface;

class MaskNumberService
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRandomMaskNumber(): string
    {
        $numbers = $this->entityManager->getRepository(MaskNumber::class)->getMaskNumbers();
        $numbers = array_column($numbers, 'number');

        return $numbers[array_rand($numbers, 1)];
    }
}
