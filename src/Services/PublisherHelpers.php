<?php

namespace App\Services;

use App\Entity\Publisher;
use App\Security\IndyCallUserInterface;
use App\Utils\DateUtils;
use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class PublisherHelpers
{
    private EntityManagerInterface $entityManager;

    private string $basePrice;

    public function __construct(EntityManagerInterface $entityManager, string $basePrice)
    {
        $this->entityManager = $entityManager;
        $this->basePrice = $basePrice;
    }

    /**
     * @param Publisher|IndyCallUserInterface $user
     * @param $gdprStatus
     *
     * @return \string[][]
     *
     * @throws \JsonException
     */
    public function updatePublisherGDPR(Publisher|IndyCallUserInterface $user, $gdprStatus): array
    {
        /** @var  IndyCallUserInterface $user */
        $publisherJSONData = $user->getJsonData();
        $newData = [
            'gdpr' => [
                'date' => Carbon::now()->toDateString(),
                'status' => (int)$gdprStatus
            ]
        ];
        $dataMerged = array_merge($publisherJSONData, $newData);
        $user->setJsonData($dataMerged);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return ['result' => 'ok'];
    }

    /**
     * @param Publisher|IndyCallUserInterface $publisher
     *
     * @return array
     * @throws \JsonException
     */
    public function getPublisherData(Publisher|IndyCallUserInterface $publisher): array
    {
        $userJsonData = $publisher->getJsonData();
        $PremPhoneDateExpire = $userJsonData['prem_phone']['date_expire'];
        $premiumExpred = new DateTime($PremPhoneDateExpire);
        $isExpired = (int) (!$premiumExpred || ($premiumExpred < Carbon::create('now')));
        $purchasedMinutes = round(($userJsonData['amount']['balance'] / $this->basePrice) * 60);
        $purchasedBalance = (float)$userJsonData['amount']['balance'];

        $arrayMerge = array_merge(
            self::BASE_PUBLISHER_DATA,
            [
                'purchasedMinutes' => $purchasedMinutes,
                'purchasedBalace' => $purchasedBalance,
                'calledAmount' => $userJsonData['amount']['called_amount'],
                'tabPurchsesEnabled' => 0,
                'premiumNumber' => [
                    'changeCount' => $userJsonData['prem_phone']['tryis'],
                    'dateValid' => $PremPhoneDateExpire
                        ? DateUtils::stringToDate($PremPhoneDateExpire)->toDateTimeString()
                        : null,
                    'number' => $userJsonData['prem_phone']['number'],
                    'expired' => $isExpired,
                    'tabNumberEnabled' => $userJsonData['prem_phone']['enabled'],
                ],
                'userArea' => [
                    'tabUserAreaEnabled' => $userJsonData['user_area']['enabled'],
                    'uniqCode' => $publisher->getId(),
                    'uniqCodeEnabled' => $userJsonData['user_area']['uniq_code_enabled'],
                    'url' => 'http://www.indycall.com'
                ],
                'gdpr' => [
                    'date' => $userJsonData['gdpr']['date'],
                    'status' => $userJsonData['gdpr']['status'],
                ],
                'debug_enabled' => $userJsonData['debug_enabled'] ?? false,
            ]
        );
        unset($arrayMerge['prem_phone']);
        return ['result' => $arrayMerge];
    }

    public const BASE_PUBLISHER_DATA = [
        "phoneNumberVerified" => 0,
        "phoneSmsTryToday" => 1,
        "tabSettingsEnabled" => 1,
        "tabPrsNumberEnabled" => 1,
        "phoneNumberIn" => "",
        "phoneNumberInMinutes" => "0",
        "country" => "",
        "purchasedMinutes" => 0,
        "purchasedBalace" => 0,
        "calledAmount" => 0,
        "tabPurchsesEnabled" => 1,
        "tabPollFishEnabled" => 1,
        "tabAnnecyEnabled" => 1,
        "prem_phone" => [
            "changeCount" => null,
            "dateValid" => null,
            "number" => "",
            "expired" => 0,
            "tabNumberEnabled" => 1
        ],
        "userArea" => [
            "tabUserAreaEnabled" => 1,
            "uniqCode" => null,
            "uniqCodeEnabled" => 1,
            "url" => "http://www.indycall.com"
        ],
        "servers" => [
            "api.indycall.com",
            "indyservice0.com",
            "indyservice0.de",
            "api.indycall.in"
        ],
        "tabCallerIdEnabled" => 1,
        "freeCallsTotal" => 0,
        "freeCallsCount" => 0,
        "userTariff" => [
            "current" => "F",
            "wait_time" => 900
        ],
        "gdpr" => [
            "date" => "",
            "status" => 0
        ],
        "callInfoScreen" => [
            "enabled" => 1
        ],
        "landing" => [
            "before_call_scr",
            "after_call_scr"
        ],
        "network" => [
            "appodeal",
            "appodeal"
        ],
        "rate_default" => 0.009,
        "app" => [
            "type" => "android"
        ]
    ];
}
