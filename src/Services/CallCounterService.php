<?php

namespace App\Services;

use App\Entity\Publisher;
use Doctrine\ORM\EntityManagerInterface;

class CallCounterService
{
    private RateService $rateService;
    private float $callMaxTime;
    private float $callMinTime;
    private float $callMaxTimePaid;
    private float $callMaxPay;
    private EntityManagerInterface $entityManager;

    /** I know float is a sheet, but they use it (kill previous developer) */
    public function __construct(
        float $callMaxTime,
        float $callMinTime,
        float $callMaxTimePaid,
        float $callMaxPay,
        RateService $rateService,
        EntityManagerInterface $entityManager
    ) {
        $this->callMaxTime = $callMaxTime;
        $this->callMinTime = $callMinTime;
        $this->callMaxTimePaid = $callMaxTimePaid;
        $this->callMaxPay = $callMaxPay;
        $this->rateService = $rateService;
        $this->entityManager = $entityManager;
    }

    public function countHowManySecondsForCallToNumber($publisherId, string $number): int
    {
        if (strlen($number) <= 6) {
            return 0;
        }
        $publisher = $this->entityManager->getRepository(Publisher::class)->find($publisherId);
        $maxTime = $this->callMaxTime;
        $minTime = $this->callMinTime;
        $maxPrice = 0;
        $publisherData = $publisher->getJsonData();
        if ($publisherData['amount']['balance'] > 0) {
            $maxTime = $this->callMaxTimePaid;
            $maxPrice = $this->callMaxPay + $publisherData['amount']['balance'];
        }
        $rate = $this->rateService->getRate($number);

        if (!$rate) {
            return 0;
        }

        $calc = ($maxPrice / $rate) * 60;

        if ($calc > $maxTime) {
            return (int) $maxTime;
        }

        if ($calc > $minTime) {
            return (int) $calc;
        }

        if($calc === 0) {
            return 1;
        }

        return 0;
    }
}
