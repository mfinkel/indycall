<?php

declare(strict_types=1);

namespace App\Traits;

use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait PaymentsTraits
{
    /**
     * @ORM\Column(name="poa_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $poaId;

    /**
     * @ORM\Column(name="poa_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private string $poaAmount;

    /**
     * @ORM\Column(name="poa_currency", type="string", length=3, nullable=false)
     */
    private string $poaCurrency;

    /**
     * @ORM\Column(name="poa_publisher_id", type="integer", nullable=false)
     */
    private int $poaPublisherId;

    /**
     * @ORM\Column(
     *     name="poa_state",
     *     type="string",
     *     length=1,
     *     nullable=false,
     *     options={"fixed"=true,"comment"="N-new, D-done, E-error, U-unknown, S-skipped, R-refund"}
     * )
     */
    private string $poaState;

    /**
     * @var DateTimeInterface|Carbon
     *
     * @ORM\Column(name="poa_date_create", type="datetime", nullable=false)
     */
    private DateTimeInterface | Carbon $poaDateCreate;

    /**
     * @var DateTimeInterface|Carbon|null
     *
     * @ORM\Column(name="poa_date_update", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private DateTimeInterface | null | Carbon $poaDateUpdate;

    /**
     * @ORM\Column(name="poa_product_id", type="string", length=45, nullable=false, options={"comment"="sku"})
     */
    private string $poaProductId;

    /**
     * @ORM\Column(name="poa_receipt", type="blob", length=65535, nullable=false)
     */
    private string $poaReceipt;

    /**
     * @ORM\Column(name="poa_order_id", type="string", length=45, nullable=false)
     */
    private string $poaOrderId;

    /**
     * @ORM\Column(name="poa_order_desc", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private ?string $poaOrderDesc = null;


    public function __construct()
    {
        $this->poaDateCreate = Carbon::create('now');
        $this->poaDateUpdate = Carbon::create('now');
    }

    public function getPoaId(): ?int
    {
        return $this->poaId;
    }

    public function getPoaAmount(): ?string
    {
        return $this->poaAmount;
    }

    public function setPoaAmount(string $poaAmount): self
    {
        $this->poaAmount = $poaAmount;

        return $this;
    }

    public function getPoaCurrency(): ?string
    {
        return $this->poaCurrency;
    }

    public function setPoaCurrency(string $poaCurrency): self
    {
        $this->poaCurrency = $poaCurrency;

        return $this;
    }

    public function getPoaPublisherId(): ?int
    {
        return $this->poaPublisherId;
    }

    public function setPoaPublisherId(int $poaPublisherId): self
    {
        $this->poaPublisherId = $poaPublisherId;

        return $this;
    }

    public function getPoaState(): ?string
    {
        return $this->poaState;
    }

    public function setPoaState(string $poaState): self
    {
        $this->poaState = $poaState;

        return $this;
    }

    public function getPoaDateCreate(): DateTimeInterface | Carbon | null
    {
        return $this->poaDateCreate;
    }

    public function setPoaDateCreate(DateTimeInterface | Carbon $poaDateCreate): self
    {
        $this->poaDateCreate = $poaDateCreate;

        return $this;
    }

    public function getPoaDateUpdate(): ?DateTimeInterface
    {
        return $this->poaDateUpdate;
    }

    public function setPoaDateUpdate(DateTimeInterface | Carbon | null $poaDateUpdate): self
    {
        $this->poaDateUpdate = $poaDateUpdate;

        return $this;
    }

    public function getPoaProductId(): ?string
    {
        return $this->poaProductId;
    }

    public function setPoaProductId(string $poaProductId): self
    {
        $this->poaProductId = $poaProductId;

        return $this;
    }

    public function getPoaReceipt(): string
    {
        return $this->poaReceipt;
    }

    public function setPoaReceipt(string $poaReceipt): self
    {
        $this->poaReceipt = $poaReceipt;

        return $this;
    }

    public function getPoaOrderId(): ?string
    {
        return $this->poaOrderId;
    }

    public function setPoaOrderId(string $poaOrderId): self
    {
        $this->poaOrderId = $poaOrderId;

        return $this;
    }

    public function getPoaOrderDesc(): ?string
    {
        return $this->poaOrderDesc;
    }

    public function setPoaOrderDesc(?string $poaOrderDesc): self
    {
        $this->poaOrderDesc = $poaOrderDesc;

        return $this;
    }
}
