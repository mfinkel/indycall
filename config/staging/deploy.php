<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{
    public function configure()
    {
        return $this->getConfigBuilder()
            ->symfonyEnvironment('prod')
            ->composerInstallFlags('--no-dev --prefer-dist --no-interaction --quiet')
            ->useSshAgentForwarding(false)
            ->server('root@135.181.35.66')
            ->deployDir('/var/www/html/indycall/')
            ->repositoryUrl('git@gitlab.com:PavelY/indycall-api-new.git')
            ->repositoryBranch('master')
            ->sharedFilesAndDirs(['var/log', 'var/cache'])
            ->fixPermissionsWithChown('www-data:www-data')
            ->keepReleases(5)
        ;
    }

    public function beforePreparing()
    {
        $this->runRemote('sudo chown -R www-data: /var/www/html/indycall/');
        $this->log('Copy env  file to deploy');
        $this->runRemote('cp {{ deploy_dir }}/repo/.env.staging {{ project_dir }}/.env');
        $this->log('Dump autoload for any reasons');
        $this->runRemote('cd {{ project_dir }} && APP_ENV=prod composer dump-autoload');
    }

    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {
        $this->log('Change owner for cache');
        $this->runRemote('sudo chown -R www-data: var/');
        $this->log('Generating certificates');
        $this->runRemote('php bin/console lexik:jwt:generate-keypair');
    }
};

