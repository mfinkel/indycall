<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210321000302 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adv_ayetstudios CHANGE aye_transaction aye_transaction VARCHAR(45) DEFAULT NULL, CHANGE aye_device_id aye_device_id VARCHAR(45) DEFAULT NULL, CHANGE aye_date aye_date DATETIME(6) DEFAULT NULL, CHANGE aye_status aye_status CHAR(1) DEFAULT \'N\' NOT NULL');
        $this->addSql('ALTER TABLE blocked_numbers CHANGE bn_phone bn_phone VARCHAR(20) DEFAULT NULL, CHANGE bn_date_bgn bn_date_bgn DATETIME(6) DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE payments_android CHANGE poa_date_update poa_date_update DATETIME(6) DEFAULT CURRENT_TIMESTAMP, CHANGE poa_order_desc poa_order_desc VARCHAR(45) DEFAULT NULL');
        $this->addSql('ALTER TABLE payments_apple CHANGE poa_date_update poa_date_update DATETIME(6) DEFAULT CURRENT_TIMESTAMP, CHANGE poa_order_desc poa_order_desc VARCHAR(45) DEFAULT NULL');
        $this->addSql('ALTER TABLE products CHANGE prod_id prod_id VARCHAR(45) NOT NULL, CHANGE prod_value prod_value VARCHAR(45) DEFAULT NULL, CHANGE prod_name prod_name VARCHAR(45) DEFAULT NULL, CHANGE prod_currency prod_currency VARCHAR(15) DEFAULT NULL, CHANGE prod_order prod_order TINYINT(1) DEFAULT NULL, CHANGE prod_type prod_type VARCHAR(255) DEFAULT NULL, CHANGE prod_comments prod_comments VARCHAR(145) DEFAULT NULL');
        $this->addSql('ALTER TABLE publishers CHANGE pub_gn_id pub_gn_id VARCHAR(60) DEFAULT NULL COMMENT \'Google ID\', CHANGE pub_gn_refresh_token pub_gn_refresh_token VARCHAR(512) DEFAULT NULL, CHANGE pub_date pub_date DATETIME(6) DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE pub_date_last pub_date_last DATETIME(6) DEFAULT CURRENT_TIMESTAMP, CHANGE pub_uniq_code pub_uniq_code VARCHAR(9) DEFAULT NULL, CHANGE pub_tmp pub_tmp VARCHAR(40) DEFAULT NULL');
        $this->addSql('ALTER TABLE purchased_phones_history CHANGE pp_date pp_date DATETIME(6) DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE pp_num_before pp_num_before VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE rates CHANGE rate_date rate_date DATETIME(6) DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE rate_prefix rate_prefix VARCHAR(6) DEFAULT NULL');
        $this->addSql('ALTER TABLE requests CHANGE req_dst_number req_dst_number VARCHAR(20) DEFAULT NULL, CHANGE req_src_number req_src_number VARCHAR(20) DEFAULT NULL, CHANGE req_state req_state VARCHAR(255) DEFAULT \'NEW\' NOT NULL');
        $this->addSql('INSERT INTO indycall.publishers (pub_id, pub_sn_id, pub_gn_id, pub_gn_token, pub_gn_refresh_token, pub_data, pub_date, pub_date_last, pub_uniq_code, pub_tmp) VALUES (1, \'moris_unit_test\', \'email@email.com\', null, null, \'{"phoneNumber":null,"phoneNumberVerified":0,"phoneSmsTryToday":1,"tabSettingsEnabled":1,"tabPrsNumberEnabled":1,"phoneNumberIn":"","phoneNumberInMinutes":"0","country":"","purchasedMinutes":0,"purchasedBalace":0,"calledAmount":0,"tabPurchsesEnabled":1,"tabPollFishEnabled":1,"tabAnnecyEnabled":1,"premiumNumber":{"changeCount":null,"dateValid":null,"number":"","expired":1,"tabNumberEnabled":1},"userArea":{"tabUserAreaEnabled":1,"uniqCode":null,"uniqCodeEnabled":1,"url":"http:\/\/www.indycall.com"},"servers":["api.indycall.com","indyservice0.com","indyservice0.de","api.indycall.in"],"tabCallerIdEnabled":1,"freeCallsTotal":0,"freeCallsCount":0,"userTariff":{"current":"F","wait_time":900},"gdpr":{"date":"","status":0},"callInfoScreen":{"enabled":1},"landing":["before_call_scr","after_call_scr"],"network":["appodeal","appodeal"],"rate_default":0.009,"app":{"type":"android"},"lastIp":"127.0.0.1","geo":{"lat":0,"lon":0},"amount":{"balance":0,"purchase_enabled":1,"called_amount":0},"prem_phone":{"number":"","date_expire":null,"enabled":1,"tryis":null},"user_area":{"enabled":1,"uniq_code_enabled":1}}\', \'2021-03-21 02:24:41.825721\', \'2021-03-21 02:24:41.825726\', null, null);');
        $this->addSql('INSERT INTO indycall.blocked_numbers (bn_id, bn_pub_id, bn_phone, bn_date_bgn, bn_date_exp) VALUES (1, 1, \'911234567891\', \'2021-03-28 00:42:23.970670\', \'2021-03-30 00:42:23.970555\')');
        $this->addSql('INSERT INTO indycall.blocked_numbers (bn_id, bn_pub_id, bn_phone, bn_date_bgn, bn_date_exp) VALUES (2, 1, \'\', \'2021-03-28 00:42:23.970670\', \'2071-03-28 00:47:14.687400\')');
        $this->addSql('INSERT INTO indycall.mask_numbers (number) VALUES (\'911234567890\');');
        $this->addSql('INSERT INTO indycall.mask_numbers (number) VALUES (\'911234567891\');');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adv_ayetstudios CHANGE aye_transaction aye_transaction VARCHAR(45) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE aye_device_id aye_device_id VARCHAR(45) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE aye_date aye_date DATETIME(6) DEFAULT NULL, CHANGE aye_status aye_status CHAR(1) CHARACTER SET utf8mb4 DEFAULT \'\'\'N\'\'\' NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE blocked_numbers CHANGE bn_phone bn_phone VARCHAR(20) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE bn_date_bgn bn_date_bgn DATETIME(6) DEFAULT \'current_timestamp(6)\'');
        $this->addSql('ALTER TABLE payments_android CHANGE poa_date_update poa_date_update DATETIME(6) DEFAULT \'current_timestamp(6)\', CHANGE poa_order_desc poa_order_desc VARCHAR(45) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE payments_apple CHANGE poa_date_update poa_date_update DATETIME(6) DEFAULT \'current_timestamp(6)\', CHANGE poa_order_desc poa_order_desc VARCHAR(45) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE products CHANGE prod_id prod_id VARCHAR(45) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prod_value prod_value VARCHAR(45) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prod_name prod_name VARCHAR(45) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prod_currency prod_currency VARCHAR(15) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prod_order prod_order TINYINT(1) DEFAULT NULL, CHANGE prod_type prod_type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prod_comments prod_comments VARCHAR(145) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE publishers CHANGE pub_gn_id pub_gn_id VARCHAR(60) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'Google ID\', CHANGE pub_gn_refresh_token pub_gn_refresh_token VARCHAR(512) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE pub_date pub_date DATETIME(6) DEFAULT \'current_timestamp(6)\' NOT NULL, CHANGE pub_date_last pub_date_last DATETIME(6) DEFAULT \'current_timestamp(6)\', CHANGE pub_uniq_code pub_uniq_code VARCHAR(9) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE pub_tmp pub_tmp VARCHAR(40) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE purchased_phones_history CHANGE pp_date pp_date DATETIME(6) DEFAULT \'current_timestamp(6)\' NOT NULL, CHANGE pp_num_before pp_num_before VARCHAR(25) CHARACTER SET utf8mb4 DEFAULT \'\'\'\'\'\'\'\'\'\'\'\'\'\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE rates CHANGE rate_date rate_date DATETIME(6) DEFAULT \'current_timestamp(6)\' NOT NULL, CHANGE rate_prefix rate_prefix VARCHAR(6) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE requests CHANGE req_dst_number req_dst_number VARCHAR(20) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE req_src_number req_src_number VARCHAR(20) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE req_state req_state VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'\'\'NEW\'\'\' NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
