<?php

declare(strict_types=1);

namespace App\Tests\Api\Publisher;

use App\Controller\ApiController;
use App\Tests\ApiTester;
use Symfony\Component\HttpFoundation\Response;

class GetUserDataCest
{
    private array $token;

    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST(
            $I->getBaseUrl(),
            [

                "id" => 154688230,
                "method" => "doRegister",
                "jsonrpc" => "2.0",
                "params" => [
                    "clientID" => "moris_unit_test",
                    "appVersion" => "1.2.8"
                ]
            ]
        );
        $this->token = $I->grabDataFromResponseByJsonPath('$.result.SecurityKey');
    }

    // tests
    public function tryToTest(ApiTester $I)
    {
    }

    public function getUserDataSuccess(ApiTester $I)
    {
        $schema = file_get_contents(__DIR__.'/userDataSchema.json', true);

        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => ApiController::GET_USER_DATA,
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function updateUserGDPR(ApiTester $I)
    {
        $schema = file_get_contents(__DIR__.'/setUserGDPR.json', true);

        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => ApiController::SET_GDPR,
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                    'Agree' => true,

                ],
            ]
        );

        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }
}
