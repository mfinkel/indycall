<?php

namespace App\Tests\api;

use App\Controller\HealthStatusController;
use App\Exception\IndyCallException;
use App\Tests\ApiTester;
use Symfony\Component\HttpFoundation\Response;

class BasicAPICest
{
    public function testHealthResponse(ApiTester $I): void
    {
        $I->sendGet('/healthStatus');
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains(
            json_encode(
                HealthStatusController::STATUS_MESSAGE,
                JSON_THROW_ON_ERROR
            )
        );
    }

    public function testApiNotFound(ApiTester $I): void
    {
        $I->expect(IndyCallException::class);
        $I->sendGet('/ApiNotFound');
        $I->seeResponseCodeIs(Response::HTTP_OK);
    }

    public function testApiExceptionScheme(ApiTester $I): void
    {
        $scheme = file_get_contents('exceptionScheme.json', true);
        $post = [
            'id' => '154688231',
            'method' => 'doChangePremNumber',
            'jsonrpc' => '2.0',
            'params' => [
                'SecurityKey' => "SecurityKey",
                'PhoneNumber' => '1234567890'
            ]
        ];
        $I->expect(IndyCallException::class);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPost('/server.ind.php', $post);
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($scheme);
    }

    public function testApiExceptionSchemeNoUserInfo(ApiTester $I): void
    {
        $scheme = file_get_contents('exceptionScheme.json', true);
        $post = [
            'id' => '154688231',
            'method' => 'blockPhoneList',
            'jsonrpc' => '2.0',
            'params' => []
        ];
        $I->expect(IndyCallException::class);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPost('/server.ind.php', $post);
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($scheme);
    }

    public function testApiMethodNotFound(ApiTester $I): void
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST(
            $I->getBaseUrl(),
            [

                "id" => 154688230,
                "method" => "doRegister",
                "jsonrpc" => "2.0",
                "params" => [
                    "clientID" => "moris_unit_test",
                    "appVersion" => "1.2.8"
                ]
            ]
        );
        $token = $I->grabDataFromResponseByJsonPath('$.result.SecurityKey');
        $scheme = file_get_contents(__DIR__.'/methodNotFound.json', true);
        $post = [
            'id' => '154688231',
            'method' => 'blockPhoneList123',
            'jsonrpc' => '2.0',
            'params' => [
                'SecurityKey' => $token[0],
            ]
        ];
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPost(
            $I->getBaseUrl(),
            $post
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($scheme);
    }
}
