<?php

namespace App\Tests\Api\BlockMethods;

use App\Tests\ApiTester;
use Symfony\Component\HttpFoundation\Response;

class BlockMethodsCest
{
    private array $token;

    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST(
            $I->getBaseUrl(),
            [

                "id" => 154688230,
                "method" => "doRegister",
                "jsonrpc" => "2.0",
                "params" => [
                    "clientID" => "moris_unit_test",
                    "appVersion" => "1.2.8"
                ]
            ]
        );
        $this->token = $I->grabDataFromResponseByJsonPath('$.result.SecurityKey');
    }

    public function blockPhoneList(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__ . '/blockPhoneList.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'blockPhoneList',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function blockPhoneAdd(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__ . '/addAndDeleteBlock.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'blockPhoneAdd',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                    'Phone' => '911234567890',
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function blockPhoneAddError(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__ . '/requestFailed.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'blockPhoneAdd',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function blockPhoneEdit(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__ . '/editBlock.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'blockPhoneEdit',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                    "PhoneSrc" => "911234567891",
                    "PhoneDst" => "911234567890"
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function blockPhoneEditError(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__ . '/requestFailed.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'blockPhoneEdit',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                    "PhoneDst" => "911234567890"
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function blockPhoneDelete(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__ . '/addAndDeleteBlock.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'blockPhoneDel',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                    'Phone' => '911234567891',
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function blockPhoneDeleteError(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__ . '/requestFailed.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'blockPhoneDel',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }
}
