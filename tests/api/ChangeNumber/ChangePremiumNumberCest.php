<?php
namespace App\Tests\Api\ChangeNumber;

use App\Tests\ApiTester;
use Symfony\Component\HttpFoundation\Response;

class ChangePremiumNumberCest
{
    private array $token;

    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST(
            $I->getBaseUrl(),
            [

                "id" => 154688230,
                "method" => "doRegister",
                "jsonrpc" => "2.0",
                "params" => [
                    "clientID" => "moris_unit_test",
                    "appVersion" => "1.2.8"
                ]
            ]
        );
        $this->token = $I->grabDataFromResponseByJsonPath('$.result.SecurityKey');
    }

    public function doChangePremiumNumberSuccess(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__.'/success.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'doChangePremNumber',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                    "PhoneNumber" => "1234567890"
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }

    public function doChangePremiumNumberError(ApiTester $I): void
    {
        $schema = file_get_contents(__DIR__.'/failed.json', true);
        $I->sendPost(
            $I->getBaseUrl(),
            [
                'id' => '154688230',
                'method' => 'doChangePremNumber',
                'jsonrpc' => '2.0',
                'params' => [
                    'SecurityKey' => $this->token[0],
                ],
            ]
        );
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseIsValidOnJsonSchemaString($schema);
    }
}
