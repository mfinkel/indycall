<?php

namespace App\Tests\Unit\Services;

use App\Entity\MaskNumber;
use App\Repository\MaskNumberRepository;
use App\Services\MaskNumberService;
use Codeception\PHPUnit\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;

class MaskNumberServiceTest extends TestCase
{
    /**
     * @var EntityManagerInterface|MockObject
     */
    private MockObject|EntityManagerInterface $entityManager;

    /**
     * @var MaskNumberRepository|MockObject
     */
    private MockObject|MaskNumberRepository $repo;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->repo = $this->createMock(MaskNumberRepository::class);
    }

    public function testGetGetMaskNumber()
    {
        $maskNumber = new MaskNumberService(
            $this->entityManager
        );
        $this->entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(MaskNumber::class)
            ->willReturn($this->repo);
        $testNumbers = ['123', '321', '132', '231'];
        $this->repo->expects(self::once())->method('getRandomNumbers')->willReturn($testNumbers);
        $result = $maskNumber->getRandomMaskNumber();
        self::assertIsString($result);
        self::assertContains($result, $testNumbers);
    }
}
