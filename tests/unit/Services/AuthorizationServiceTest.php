<?php

namespace App\Tests\Unit\Services;

use App\Entity\Publisher;
use App\Exception\IndyCallGeneralException;
use App\Exception\PublisherIssue;
use App\Services\AuthorizationService;
use App\Services\GoogleOauthClient;
use Codeception\Test\Unit;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

class AuthorizationServiceTest extends Unit
{
    /**
     * @var MockObject|LoggerInterface
     */
    private MockObject|LoggerInterface $loggerInterface;

    /**
     * @var EntityManagerInterface|MockObject
     */
    private MockObject|EntityManagerInterface $entityManager;

    /**
     * @var GoogleOauthClient|MockObject
     */
    private GoogleOauthClient|MockObject $googleOauthClient;

    /**
     * @var JWTManager|MockObject
     */
    private JWTManager|MockObject $jwtManager;

    /**
     * @var Publisher|MockObject
     */
    private MockObject|Publisher $publisher;

    /**
     * @var ObjectRepository|MockObject
     */
    private MockObject|ObjectRepository $repo;

    protected function _setUp(): void
    {
        $this->publisher = $this->createMock(Publisher::class);

        $this->loggerInterface = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityManager = $this->createMock(EntityManagerInterface::class);

        $this->googleOauthClient = $this->getMockBuilder(GoogleOauthClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->jwtManager = $this->getMockBuilder(JWTManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->repo = $this->createMock(ObjectRepository::class);
    }

    public function testAuthorizeThrowException(): void
    {
        $authorizationService = new AuthorizationService(
            $this->loggerInterface,
            $this->entityManager,
            $this->jwtManager,
            $this->googleOauthClient,
        );
        $this->expectException(PublisherIssue::class);
        $this->expectExceptionMessage('Publisher not saved');
        $authorizationService->authorize(['params' => []], '127.0.0.1');
    }

    public function testAuthorizeByClientIdExistInDb(): void
    {
        $authorizationService = new AuthorizationService(
            $this->loggerInterface,
            $this->entityManager,
            $this->jwtManager,
            $this->googleOauthClient,
        );
        $this->entityManager->expects(self::once())->method('getRepository')->willReturn($this->repo);
        $this->repo->expects(self::once())->method('findOneBy')->with(["serialNumber" => 'some_string'])->willReturn(
            $this->publisher
        );

        $this->publisher->expects(self::once())->method('getId')->willReturn(1);
        $this->jwtManager->expects(self::once())->method('create')->with($this->publisher)->willReturn('');
        $authorizationService->authorize(['params' => ['clientID' => 'some_string']], '127.0.0.1');
    }

    //TODO - fix unit test
    public function testAuthorizeByNewClientId(): void
    {
        $authorizationService = new AuthorizationService(
            $this->loggerInterface,
            $this->entityManager,
            $this->jwtManager,
            $this->googleOauthClient,
        );
        $this->entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($this->repo);
        $this->repo
            ->expects(self::once())
            ->method('findOneBy')
            ->with(["serialNumber" => 'some_string'])
            ->willReturn(null);
        $this->entityManager
            ->expects(self::once())
            ->method('persist');
        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $result = $authorizationService->authorize(['params' => ['clientID' => 'some_string']], '127.0.0.1');
        self::assertIsArray($result);
        self::assertArrayHasKey('result', $result);
        self::assertArrayHasKey('SecurityKey', $result['result']);
    }

    //TODO - fix unit test
    public function testAuthorizeByClientIdNotFound(): void
    {
        $authorizationService = new AuthorizationService(
            $this->loggerInterface,
            $this->entityManager,
            $this->jwtManager,
            $this->googleOauthClient,
        );
        $this->entityManager->expects(self::once())->method('getRepository')->willReturn($this->repo);
        $this->repo->expects(self::once())->method('findOneBy')->willReturn(null);

        $authorizationService->authorize(['params' => ['clientID' => 'some_string']], '127.0.0.1');
    }

    public function testAuthorizeByGoogleExistInDataBase(): void
    {
        $authorizationService = new AuthorizationService(
            $this->loggerInterface,
            $this->entityManager,
            $this->jwtManager,
            $this->googleOauthClient,
        );

        $this->googleOauthClient
            ->expects(self::once())
            ->method('exchangeCodeToAccessToken')
            ->willReturn(
                [
                    'token' => 'token',
                    'refreshToken' => 'refreshToken',
                    'id' => 'id',
                    'email' => 'email@email.com',
                ]
            );
        $this->entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($this->repo);
        $this->repo
            ->expects(self::once())
            ->method('findOneBy')
            ->with(["googleId" => 'email@email.com'])
            ->willReturn($this->publisher);
        $result = $authorizationService->authorize(['params' => ['authCode' => 'some_string']], '127.0.0.1');
        self::assertIsArray($result);
        self::assertArrayHasKey('result', $result);
        self::assertArrayHasKey('SecurityKey', $result['result']);
    }

    public function testAuthorizeByNewGoogle(): void
    {
        $authorizationService = new AuthorizationService(
            $this->loggerInterface,
            $this->entityManager,
            $this->jwtManager,
            $this->googleOauthClient,
        );

        $this->googleOauthClient
            ->expects(self::once())
            ->method('exchangeCodeToAccessToken')
            ->willReturn(
                [
                    'token' => 'token',
                    'refreshToken' => 'refreshToken',
                    'id' => 'id',
                    'email' => 'email@email.com',
                ]
            );
        $this->entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($this->repo);
        $this->repo
            ->expects(self::once())
            ->method('findOneBy')
            ->with(["googleId" => 'email@email.com'])
            ->willReturn(null);
        $this->entityManager
            ->expects(self::once())
            ->method('persist');
        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $result = $authorizationService->authorize(['params' => ['authCode' => 'some_string']], '127.0.0.1');
        self::assertIsArray($result);
        self::assertArrayHasKey('result', $result);
        self::assertArrayHasKey('SecurityKey', $result['result']);
    }

    public function testRefreshCodeException()
    {
        $authorizationService = new AuthorizationService(
            $this->loggerInterface,
            $this->entityManager,
            $this->jwtManager,
            $this->googleOauthClient,
        );
        $this->expectException(IndyCallGeneralException::class);
        $this->expectExceptionMessage(IndyCallGeneralException::MESSAGE);
        $this->expectExceptionCode(IndyCallGeneralException::CODE);
        $authorizationService->authorize(['params' => ['refreshCode' => 'some_string']], '127.0.0.1');
    }
}
