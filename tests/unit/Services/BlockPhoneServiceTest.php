<?php

namespace App\Tests\Unit\Services;

use App\Entity\BlockedNumber;
use App\Entity\Publisher;
use App\Exception\EmptyPhone;
use App\Exception\PurchaseBeforeAddBlock;
use App\Repository\BlockedNumberRepository;
use App\Services\BlockPhoneService;
use Codeception\PHPUnit\TestCase;
use Doctrine\Common\Cache\Cache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;

class BlockPhoneServiceTest extends TestCase
{
    /**
     * @var EntityManagerInterface|MockObject
     */
    private EntityManagerInterface|MockObject $entityManager;

    /**
     * @var Publisher|MockObject
     */
    private MockObject|Publisher $publisher;

    private MockObject|BlockedNumberRepository $repo;

    private MockObject|BlockedNumber $blockNumber;

    private $configuration;

    private Cache|MockObject $cache;

    public function setUp(): void
    {
        $this->publisher = $this->createMock(Publisher::class);
        $this->blockNumber = $this->createMock(BlockedNumber::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->repo = $this->createMock(BlockedNumberRepository::class);
        $this->configuration = $this->createMock(Configuration::class);
        $this->cache = $this->createMock(Cache::class);
    }

    public function testGetBlockNumberListEmptyList(): void
    {
        $this->entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(BlockedNumber::class)
            ->willReturn($this->repo);

        $this->repo->expects(self::once())->method('getBlockedNumberList');
        $this->repo->expects(self::once())->method('countPurchasedSlots');

        $blockNumberService = new BlockPhoneService($this->entityManager);
        $result = $blockNumberService->blockPhoneList($this->publisher);
        self::assertIsArray($result);
        self::assertArrayHasKey('result', $result);
        self::assertEmpty($result['result']);
    }

    public function testGetBlockNumberList(): void
    {
        $this->entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(BlockedNumber::class)
            ->willReturn($this->repo);

        $this->repo->expects(self::once())->method('getBlockedNumberList')->with($this->publisher)->willReturn([['blockedNumber' => '123123132']]);
        $this->repo->expects(self::once())->method('countPurchasedSlots')->with($this->publisher)->willReturn([['blockedNumber' => '123123132']]);

        $blockNumberService = new BlockPhoneService($this->entityManager);
        $result = $blockNumberService->blockPhoneList($this->publisher);
        self::assertSame(
            $result,
            [
                'result' => [
                    'phones' => ['123123132'],
                    'total_count' => 1,
                    'free_count' => 1,
                ]
            ]
        );
    }

    public function testGetBlockNumberAddException(): void
    {
        $this->entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->with(BlockedNumber::class)
            ->willReturn($this->repo);

        $this->repo->expects(self::once())->method('countAvailableSlots')->with($this->publisher)->willReturn([$this->blockNumber]);
        $this->repo->expects(self::once())->method('countPurchasedSlots')->with($this->publisher)->willReturn([]);
        $number = '123123132';
        $this->expectException(PurchaseBeforeAddBlock::class);
        $this->expectExceptionMessage(PurchaseBeforeAddBlock::MESSAGE);
        $this->expectExceptionCode(PurchaseBeforeAddBlock::CODE);
        $blockNumberService = new BlockPhoneService($this->entityManager);
        $blockNumberService->blockPhoneAdd($this->publisher, $number);
    }

    public function testGetBlockNumberAddPremium(): void
    {
        $this->entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->with(BlockedNumber::class)
            ->willReturn($this->repo);

        $this->entityManager
            ->expects(self::once())
            ->method('getConfiguration')
            ->willReturn($this->configuration);

        $this->configuration
            ->expects(self::once())
            ->method('getResultCacheImpl')
            ->willReturn($this->cache);

        $this->cache
            ->expects(self::exactly(3))
            ->method('delete');
        $this->publisher->expects(self::exactly(4))->method('getId')->willReturn(1);
        $this->repo->expects(self::once())->method('countAvailableSlots')->with($this->publisher)->willReturn([]);
        $this->repo->expects(self::once())->method('countPurchasedSlots')->with($this->publisher)->willReturn([]);
        $number = '123123132';
        $blockNumberService = new BlockPhoneService($this->entityManager);
        $blockNumberService->blockPhoneAdd($this->publisher, $number);
    }

    public function testGetBlockNumberAddRegular(): void
    {
        $this->entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->with(BlockedNumber::class)
            ->willReturn($this->repo);

        $this->entityManager
            ->expects(self::once())
            ->method('persist');
        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $this->entityManager
            ->expects(self::once())
            ->method('getConfiguration')
            ->willReturn($this->configuration);

        $this->configuration
            ->expects(self::once())
            ->method('getResultCacheImpl')
            ->willReturn($this->cache);

        $this->cache
            ->expects(self::exactly(3))
            ->method('delete');

        $this->repo->expects(self::once())->method('countAvailableSlots')->with($this->publisher)->willReturn([$this->blockNumber]);
        $this->repo->expects(self::once())->method('countPurchasedSlots')->with($this->publisher)->willReturn([$this->blockNumber, $this->blockNumber]);
        $number = '123123132';
        $blockNumberService = new BlockPhoneService($this->entityManager);
        $blockNumberService->blockPhoneAdd($this->publisher, $number);
    }

    public function testBlockNumberEdit(): void
    {
        $this->entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(BlockedNumber::class)
            ->willReturn($this->repo);

        $this->repo->expects(self::once())->method('findOneBy');
        $phoneSrc = '123123132';
        $phoneDst = '123123131';
        $this->expectException(EmptyPhone::class);
        $this->expectExceptionMessage(EmptyPhone::MESSAGE);
        $this->expectExceptionCode(EmptyPhone::CODE);
        $blockNumberService = new BlockPhoneService($this->entityManager);
        $blockNumberService->blockPhoneEdit($this->publisher, $phoneSrc, $phoneDst);
    }
}
