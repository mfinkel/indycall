<?php

namespace App\Tests\Unit\Services;

use App\Exception\ChangeNumberAttemptsExceeded;
use App\Exception\ProductExpired;
use App\Exception\SameNumberChange;
use App\Security\IndyCallUserInterface;
use App\Services\PremiumNumberService;
use App\Utils\PhoneUtils;
use Carbon\Carbon;
use Codeception\PHPUnit\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;

class PremiumNumberServiceTest extends TestCase
{
    /**
     * @var EntityManagerInterface|MockObject
     */
    private EntityManagerInterface|MockObject $entityManager;

    /**
     * @var PhoneUtils
     */
    private PhoneUtils $utils;

    /**
     * @var IndyCallUserInterface|MockObject
     */
    private IndyCallUserInterface|MockObject $user;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->utils = $this->createMock(PhoneUtils::class);
        $this->user = $this->createMock(IndyCallUserInterface::class);
    }

    public function testAttemptsException(): void
    {
        $numberService = new PremiumNumberService(
            $this->entityManager,
            $this->utils
        );
        $this->user->expects(self::once())->method('getJsonData')->willReturn(
            '{"prem_phone":{"number":"","date_expire":null,"enabled":1,"tryis":-1}}'
        );
        $this->expectException(ChangeNumberAttemptsExceeded::class);
        $this->expectExceptionMessage(ChangeNumberAttemptsExceeded::MESSAGE);
        $this->expectExceptionCode(ChangeNumberAttemptsExceeded::CODE);

        $numberService->updateNumber($this->user, '');
    }

    public function testDateExpireException(): void
    {
        $numberService = new PremiumNumberService(
            $this->entityManager,
            $this->utils
        );
        $date = Carbon::yesterday()->toDateString();
        $data = [
            'prem_phone' => [
                'number' => '',
                'date_expire' => $date,
                'enabled' => 1,
                'tryis' => 5
            ]
        ];
        $this->user->expects(self::once())->method('getJsonData')->willReturn(json_encode($data, JSON_THROW_ON_ERROR));
        $this->expectException(ProductExpired::class);
        $this->expectExceptionMessage(ProductExpired::MESSAGE);
        $this->expectExceptionCode(ProductExpired::CODE);

        $numberService->updateNumber($this->user, '');
    }

    public function testSameNumberException(): void
    {
        $numberService = new PremiumNumberService(
            $this->entityManager,
            $this->utils
        );
        $date = Carbon::tomorrow()->toDateString();
        $data = [
            'prem_phone' => [
                'number' => '',
                'date_expire' => $date,
                'enabled' => 1,
                'tryis' => 5
            ]
        ];
        $this->user->expects(self::once())->method('getJsonData')->willReturn(json_encode($data, JSON_THROW_ON_ERROR));
        $this->expectException(SameNumberChange::class);
        $this->expectExceptionMessage(SameNumberChange::MESSAGE);
        $this->expectExceptionCode(SameNumberChange::CODE);

        $numberService->updateNumber($this->user, '');
    }

    public function testChangeNumberSuccess(): void
    {
        $numberService = new PremiumNumberService(
            $this->entityManager,
            $this->utils
        );
        $date = Carbon::tomorrow()->toDateString();
        $data = [
            'prem_phone' => [
                'number' => '123123123',
                'date_expire' => $date,
                'enabled' => 1,
                'tryis' => 5
            ]
        ];
        $newNumber = '321321321';
        $testResult = [
            'result' => [
                'changeCount' => $data['prem_phone']['tryis'] -1,
                'dateValid' => $date,
                'number' => $newNumber,
            ]
        ];

        $this->user->expects(self::once())->method('getJsonData')->willReturn(json_encode($data, JSON_THROW_ON_ERROR));
        $this->user->expects(self::once())->method('getId')->willReturn(random_int(0, 99));
        $this->entityManager->expects(self::exactly(2))->method('persist');
        $this->entityManager->expects(self::exactly(2))->method('flush');

        $this->utils->expects(self::once())->method('preparePhoneNumber')->willReturn($newNumber);

        $result = $numberService->updateNumber($this->user, $newNumber);
        self::assertSame($testResult, $result);
    }
}
