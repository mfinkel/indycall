<?php

namespace App\Tests\Unit\Services;

use App\Services\AnalyticsClient;
use App\Tests\UnitTester;
use Codeception\Test\Unit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AnalyticsClientTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    // tests
    public function testAnalyticSendRequest(): void
    {
        $client = $this->getMockBuilder(HttpClientInterface::class)->disableOriginalConstructor()->getMock();
        $baseUrl = 'http://some.url.com/some_link';
        $userId = '1';
        $methodName = 'testName';
        $userIp = '127.0.0.1';
        $analyticsClient = new AnalyticsClient($client, $baseUrl);
        $client->expects(self::once())->method('request')->with(
            Request::METHOD_GET,
            $baseUrl,
            [
                'query' => [
                    'v' => 1,
                    't' => 'pageview',
                    'tid' => 'UA-70081921-7',
                    'cid' => $userId,
                    'dp' => $methodName,
                    'uip' => $userIp
                ]
            ]
        );
        $analyticsClient->sendAnalytics($userId, $methodName, $userIp);
    }
}
