<?php

namespace App\Tests\Unit\Services;

use App\Exception\GoogleAuthenticationFailed;
use App\Services\GoogleOauthClient;
use Codeception\PHPUnit\TestCase;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use PHPUnit\Framework\MockObject\MockObject;

class GoogleOauthClientTest extends TestCase
{
    private ClientRegistry|MockObject $clientRegistry;

    private MockObject|OAuth2ClientInterface $client;

    private AbstractProvider|MockObject $provider;

    private AccessTokenInterface|MockObject $token;

    private GoogleUser|MockObject $resourceOwner;

    public function setUp(): void
    {
        $this->clientRegistry = $this->createMock(ClientRegistry::class);
        $this->client = $this->createMock(OAuth2ClientInterface::class);
        $this->provider = $this->createMock(AbstractProvider::class);
        $this->token = $this->createMock(AccessToken::class);
        $this->resourceOwner = $this->createMock(GoogleUser::class);
    }

    public function testExchangeCodeToAccessTokenetClientSuccess()
    {
        $client = new GoogleOauthClient($this->clientRegistry);
        $this->clientRegistry
            ->expects(self::exactly(2))
            ->method('getClient')
            ->with('google_main')
            ->willReturn($this->client);
        $this->provider
            ->expects(self::once())
            ->method('getAccessToken')
            ->with(
                'authorization_code',
                [
                    'code' => 'some_refresh_token_string',
                    'scope' => 'email openid',
                    'redirect_uri' => 'http://127.0.0.1:8000'
                ]
            )
            ->willReturn($this->token);

        $this->client->expects(self::once())->method('getOAuth2Provider')->willReturn($this->provider);
        $this->provider->expects(self::once())->method('getAccessToken')->willReturn($this->token);
        $this->client->expects(self::once())->method('fetchUserFromToken')->willReturn($this->resourceOwner);

        $this->token->expects(self::once())->method('getValues')->willReturn(['id_token' => 'someGoogleToken']);

        $client->exchangeCodeToAccessToken('some_refresh_token_string');
    }

    public function testExchangeCodeToAccessTokenetClientFailed()
    {
        $client = new GoogleOauthClient($this->clientRegistry);
        $this->clientRegistry
            ->expects(self::exactly(2))
            ->method('getClient')
            ->with('google_main')
            ->willReturn($this->client);
        $this->provider
            ->expects(self::once())
            ->method('getAccessToken')
            ->with(
                'authorization_code',
                [
                    'code' => 'some_refresh_token_string',
                    'scope' => 'email openid',
                    'redirect_uri' => 'http://127.0.0.1:8000'
                ]
            )
            ->willReturn($this->token);

        $this->client->expects(self::once())->method('getOAuth2Provider')->willReturn($this->provider);
        $this->provider->expects(self::once())->method('getAccessToken')->willReturn($this->token);
        $this->client->expects(self::once())->method('fetchUserFromToken')->willReturn($this->resourceOwner);
        $this->expectException(GoogleAuthenticationFailed::class);
//        $this->token->expects(self::once())->method('getValues')->willReturn(['id_token' => 'someGoogleToken']);

        $client->exchangeCodeToAccessToken('some_refresh_token_string');
    }
}
