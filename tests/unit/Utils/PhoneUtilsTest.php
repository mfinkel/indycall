<?php

namespace App\Tests\Unit\Utils;

use App\Utils\PhoneUtils;
use Codeception\PHPUnit\TestCase;

class PhoneUtilsTest extends TestCase
{
    public function testPreparePhoneNumber(): void
    {
        $phoneUtils = new PhoneUtils();
        $result = $phoneUtils->preparePhoneNumber('123-123-123-123');
        self::assertSame('123123123123', $result);

        $result = $phoneUtils->preparePhoneNumber('123 | 123-123 \ 123');
        self::assertSame('123123123123', $result);
    }
}
